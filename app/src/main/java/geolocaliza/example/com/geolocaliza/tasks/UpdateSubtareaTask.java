package geolocaliza.example.com.geolocaliza.tasks;

import android.os.AsyncTask;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import geolocaliza.example.com.geolocaliza.listeners.SubtareasCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.utils.Constants;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class UpdateSubtareaTask extends AsyncTask<Double, Double, Boolean> {


    private int task;
    private int staffId;
    private int complete;
    private int subtask;
    private int master_client;

    private ArrayList<Subtarea> subtareas = new ArrayList<Subtarea>();
    SubtareasCallbackListener callbackInterface;



    public UpdateSubtareaTask(SubtareasCallbackListener callbackInterface, int complete, int task, int staffId, int master_client, int id) {
        this.complete =complete;
        this.task =task;
        this.subtask =id;
        this.staffId =staffId;
        this.master_client =master_client;
        this.callbackInterface = callbackInterface;
    }


    private String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        this.callbackInterface.callbackUpdateSubtask(result, subtareas);
    }


    @Override
    protected Boolean doInBackground(Double... params) {
        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(Constants.API_URL+"updateCompleted/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod("POST");

            conn.setDoInput(true);
            conn.setDoOutput(true);
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("taskid", task);
            postDataParams.put("subtask", subtask);
            postDataParams.put("complete", complete);
            postDataParams.put("usuario_id", staffId);
            postDataParams.put("master_client", master_client);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();


            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + getPostDataString(postDataParams));
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());


            writer.close();
            os.close();

            conn.connect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wrapData(response);
    }


    private boolean wrapData(StringBuffer response) {
       Boolean result = true;

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(response.toString()).getAsJsonObject();

            if( jsonObject.get("success").toString().isEmpty()){
                result = false;
            }


            for (JsonElement subtareaJson :
                    jsonObject.get("subtareas").getAsJsonArray()) {
                String description = subtareaJson.getAsJsonObject().get("description").toString().replace("\"", "");
                int id = Integer.parseInt(subtareaJson.getAsJsonObject().get("id").toString().replace("\"", ""));
                int completeInt = Integer.valueOf(subtareaJson.getAsJsonObject().get("complete").toString().replace("\"", ""));
                int finishedInt = Integer.valueOf(subtareaJson.getAsJsonObject().get("finished").toString().replace("\"", ""));
                boolean finished = finishedInt==1?true:false;
                boolean complete = completeInt==1?true:false;
                Subtarea subtarea = new Subtarea(id
                        ,description, finished, complete, task);
                subtareas.add(subtarea);



            }


        }catch (Exception e){

            result = false;

        }

        return result;


    }

}