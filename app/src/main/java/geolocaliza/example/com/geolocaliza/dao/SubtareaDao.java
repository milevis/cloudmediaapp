package geolocaliza.example.com.geolocaliza.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;

@Dao
public interface SubtareaDao {

    @Query("SELECT * FROM subtarea")
    List<Subtarea> getAll();

    @Query("SELECT * FROM subtarea WHERE id IN (:ids)")
    List<Subtarea> loadAllByIds(int[] ids);


    @Query("SELECT * FROM subtarea WHERE id = (:id)")
    List<Subtarea>  loadAllById(int id);

    @Query("SELECT * FROM subtarea WHERE instalacion_id LIKE :instalacion_id")
    List<Subtarea> findAllByInstalacion(int instalacion_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Subtarea> subtareas);

    @Delete
    void delete(Subtarea subtarea);

    @Update
    public void updateSubtarea(Subtarea... subtarea);
}
