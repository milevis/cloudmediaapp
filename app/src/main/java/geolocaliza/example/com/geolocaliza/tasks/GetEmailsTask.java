package geolocaliza.example.com.geolocaliza.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.listeners.TecnicosCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Staff;
import geolocaliza.example.com.geolocaliza.utils.Constants;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class GetEmailsTask extends AsyncTask<Double, Double,  ArrayList<Staff>> {

    private String email;
    private Exception exception;

    TecnicosCallbackListener callbackInterface;

    public GetEmailsTask(TecnicosCallbackListener callbackInterface) {

        super();
        this.callbackInterface = callbackInterface;
        // do stuff
    }

    @Override
    protected void onPostExecute( ArrayList<Staff> result) {
        super.onPostExecute(result);
        this.callbackInterface.getTecnicosCallback(result);
    }



    @Override
    protected  ArrayList<Staff> doInBackground(Double... params) {


        StringBuffer response = new StringBuffer();
        try {




            URL url = new URL(Constants.API_URL+"getStaffsApp");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod(Constants.METHOD_GET);

            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.flush();


            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'get' request to URL : " + url);

            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;


            int i = 0;
            while ((inputLine = in.readLine()) != null) {
                   response.append(inputLine);

                i++;
            }
            in.close();

            System.out.println(response.toString());


            writer.close();
            os.close();

            conn.connect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wrapData(response);
    }

    private  ArrayList<Staff> wrapData(StringBuffer response){



        ArrayList<Staff> staffArrayList=new ArrayList<>();

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject objectFromString = jsonParser.parse(response.toString()).getAsJsonObject();


            for (JsonElement jsonElement :
                    objectFromString.get("success").getAsJsonArray()) {
                Staff staff = new Staff();
                staff.setEmail(jsonElement.getAsJsonObject().get("email").toString().replace("\"", ""));
                staff.setStaffname(jsonElement.getAsJsonObject().get("staffname").toString().replace("\"", ""));
                staff.setId(Integer.parseInt(jsonElement.getAsJsonObject().get("id").toString().replace("\"", "")));
                staff.setMaster_client(Integer.parseInt(jsonElement.getAsJsonObject().get("master_client").toString().replace("\"", "")));
                staffArrayList.add(staff);
            }

        }catch (Exception e){
            Log.e("Get emails task", e.getMessage());
        }
        finally {
            return  staffArrayList;
        }



    }
}