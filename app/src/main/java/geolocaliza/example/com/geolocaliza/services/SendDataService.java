package geolocaliza.example.com.geolocaliza.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import geolocaliza.example.com.geolocaliza.tasks.*;
import geolocaliza.example.com.geolocaliza.MainActivity;
import geolocaliza.example.com.geolocaliza.R;

public class SendDataService extends Service {
    private final LocalBinder mBinder = new LocalBinder();
    protected Handler handler;
    protected Toast mToast;
    static final int NOTIFICATION_ID = 543;

    private Timer timer;


    public static boolean isServiceRunning = false;

    public class LocalBinder extends Binder {
        public SendDataService getService() {
            return SendDataService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        //  super.onCreate();
        startServiceWithNotification();

    }

    @Override
    public void onDestroy() {
        isServiceRunning = false;
        super.onDestroy();


        Intent broadcastIntent = new Intent("ac.in.ActivityRecognition.RestartSensor");
        sendBroadcast(broadcastIntent);
        stoptimertask();


    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    void startServiceWithNotification() {
        if (isServiceRunning) return;
        isServiceRunning = true;

        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        notificationIntent.setAction("SENDDATA");  // A string containing the action name
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.common_full_open_on_phone);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))
                // .setContentText("Compartiendo ubicación en tiempo real")
                //.setSmallIcon(R.drawable.my_icon)
                //  .setContentTitle("Compartiendo ubicación en tiempo real")
                //.setTicker("Next alarm is set to ring on")
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(contentPendingIntent)
                .setOngoing(true)
                .setAutoCancel(false)
                .setDeleteIntent(contentPendingIntent)  // if needed
                .build();
        notification.flags = notification.flags | Notification.FLAG_NO_CLEAR;     // NO_CLEAR makes the notification stay when the user performs a "delete all" command
        startForeground(NOTIFICATION_ID, notification);
    }

    void stopMyService() {
        stopForeground(true);
        stopSelf();
        isServiceRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        final double latitude= intent.getDoubleExtra("latitude", 0);
        final double longitude = intent.getDoubleExtra("longitude", 0);
        final String email = intent.getStringExtra("email");
        final int id = intent.getIntExtra("id", 0);
        final int master_client = intent.getIntExtra("master_client", 0);
        timer(latitude, longitude, email, id, master_client);


        if (intent != null && intent.getAction().equals("SENDDATA")) {
            startServiceWithNotification();
        } else stopMyService();

        return START_STICKY;


    }



    private void timer(Double latitude, Double longitude, String email, int id, int master_client) {


        final Handler handler = new Handler();
        Timer timer = new Timer();
        final double latitude2 = latitude;
        final double longitude2 = longitude;
        final String email2 = email;
        final int id2 = id;
        final int master_client2 = master_client;



        TimerTask doAsynchronousTask = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {

                            new SendLocationTask(email2, id2, master_client2).execute(latitude2, longitude2);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            Log.d("SEND LOCATION TASK", "Imposible enviar ubicacion "+e.getMessage());
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, Long.parseLong("30000"));


        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.

    }

}