package geolocaliza.example.com.geolocaliza.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.net.Uri;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by Jhainey on 30/05/2018.
 */

@Entity(indices = {@Index("instalacion_id")}, foreignKeys = @ForeignKey(entity = Instalacion.class,
        parentColumns = "id",
        childColumns = "instalacion_id",
        onDelete = CASCADE))
public class Foto {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String url;
    private String fecha;
    private String name;
    private String image_str_base_64;

    @Ignore
    private Bitmap bitmap;
    @Ignore
    private Uri uri;
    private int instalacion_id;

    public String getImage_str_base_64() {
        return image_str_base_64;
    }

    public void setImage_str_base_64(String image_str_base_64) {
        this.image_str_base_64 = image_str_base_64;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Foto(String url, int instalacion_id, String fecha, String name, String image_str_base_64) {
        this.url = url;
        this.instalacion_id = instalacion_id;
        this.fecha = fecha;
        this.name = name;
        this.image_str_base_64 = image_str_base_64;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getInstalacion_id() {
        return instalacion_id;
    }

    public void setInstalacion_id(int instalacion_id) {
        this.instalacion_id = instalacion_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }
}
