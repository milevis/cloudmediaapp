package geolocaliza.example.com.geolocaliza.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import geolocaliza.example.com.geolocaliza.R;

import geolocaliza.example.com.geolocaliza.TextView_Lato;
import geolocaliza.example.com.geolocaliza.Textview_lato_thin;
import geolocaliza.example.com.geolocaliza.listeners.InstalationCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;


public class InstalacionesRecyclerViewAdapter extends RecyclerView.Adapter<InstalacionesRecyclerViewAdapter.ViewHolder> {

    private final List<Instalacion> mValues;
    private final InstalationCallbackListener mListener;

    public InstalacionesRecyclerViewAdapter(ArrayList<Instalacion> items, InstalationCallbackListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_instalaciones, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(String.valueOf(mValues.get(position).getId()));
        holder.mContentView.setText(mValues.get(position).getNameSurname());
        holder.instalcionDate.setText(mValues.get(position).getFechaInstalacion());
        holder.address.setText(mValues.get(position).getDireccion());
        holder.planName.setText(mValues.get(position).getNombre_servicio());

        if (mValues.size()>0) {

            holder.emptyView.setVisibility(View.GONE);

        } else {


            holder.emptyView.setVisibility(View.VISIBLE);
        }

        if(mValues.get(position).getEstatus_plan().equals("CANCELADO")){
            holder.orange.setVisibility(View.VISIBLE);
            holder.orange_circle.setVisibility(View.VISIBLE);
            holder.cancelado.setVisibility(View.VISIBLE);
            holder.green.setVisibility(View.GONE);
            holder.green_circle.setVisibility(View.GONE);
            holder.instalado.setVisibility(View.GONE);
        }else{
            holder.orange.setVisibility(View.GONE);
            holder.orange_circle.setVisibility(View.GONE);
            holder.cancelado.setVisibility(View.GONE);
            holder.green.setVisibility(View.VISIBLE);
            holder.green_circle.setVisibility(View.VISIBLE);
            holder.instalado.setVisibility(View.VISIBLE);
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.callbackClickInstalacion(holder.mItem, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final View orange;
        public final View green;
        public final View orange_circle;
        public final View green_circle;
        public final TextView mIdView;
        public final TextView_Lato cancelado;
        public final TextView_Lato planName;
        public final Textview_lato_thin instalcionDate;
        public final Textview_lato_thin address;
        public final TextView_Lato instalado;
        public final TextView mContentView;
        public final TextView emptyView;
        public Instalacion mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            orange = (View) view.findViewById(R.id.orange);
            instalcionDate = (Textview_lato_thin) view.findViewById(R.id.instalcionDate);
            address = (Textview_lato_thin) view.findViewById(R.id.address);
            orange_circle = (View) view.findViewById(R.id.orange_circle);
            green = (View) view.findViewById(R.id.green);
            green_circle = (View) view.findViewById(R.id.gree_circle);
            mIdView = (TextView) view.findViewById(R.id.id);
            cancelado = (TextView_Lato) view.findViewById(R.id.cancelado);
            planName = (TextView_Lato) view.findViewById(R.id.planName);
            instalado = (TextView_Lato) view.findViewById(R.id.instalado);
            mContentView = (TextView) view.findViewById(R.id.content);
            emptyView = (TextView) view.findViewById(R.id.empty_view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
