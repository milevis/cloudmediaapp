package geolocaliza.example.com.geolocaliza.dao;

import android.arch.persistence.room.*;

import java.util.ArrayList;
import java.util.List;

import geolocaliza.example.com.geolocaliza.entity.Instalacion;

@Dao
public interface InstalacionDao {

    @Query("SELECT * FROM instalacion")
    List<Instalacion> getAll();

    @Query("SELECT * FROM instalacion WHERE id IN (:ids)")
    List<Instalacion> loadAllByIds(int[] ids);


    @Query("SELECT * FROM instalacion WHERE id = (:id)")
    Instalacion loadById(int id);

    @Query("SELECT * FROM instalacion WHERE estatus_plan LIKE :estatus_plan LIMIT 1")
    Instalacion findByStatus(String estatus_plan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Instalacion> instalacions);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertInstalacion(Instalacion... instalacion);

    @Delete
    void delete(Instalacion instalacion);

    @Update
    public void updateInstalacion(Instalacion... instalacion);
}
