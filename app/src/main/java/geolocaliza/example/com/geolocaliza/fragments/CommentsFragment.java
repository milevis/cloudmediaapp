package geolocaliza.example.com.geolocaliza.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import geolocaliza.example.com.geolocaliza.InstalacionActivity;
import geolocaliza.example.com.geolocaliza.R;
import geolocaliza.example.com.geolocaliza.adapters.CommentsFragmentRecyclerViewAdapter;

import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.listeners.ComentariosCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ComentariosCallbackListener}
 * interface.
 */
public class CommentsFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private ComentariosCallbackListener mListener;
    private TextView emptyView;
    private List<Comentario> comentarios = new ArrayList<Comentario>();
    private RecyclerView recyclerView;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CommentsFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CommentsFragment newInstance(int columnCount) {
        CommentsFragment fragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_commentsfragment_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        this.emptyView = (TextView) view.findViewById(R.id.empty_view);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogAddComment(view.getContext());
            }
        });

        // Set the adapter
        if (recyclerView instanceof RecyclerView) {
            Context context = view.getContext();

            final Instalacion instalacion = ((InstalacionActivity) context).getInstalacion();


            recyclerView.setLayoutManager(new LinearLayoutManager(context));


            final InstalacionActivity activity = (InstalacionActivity) getActivity();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                        final List<Comentario> comentarioslist = (ArrayList<Comentario>) activity.getDatabase().comentarioDao().findAllByInstalacion(instalacion.getId());
                        comentarios.addAll(comentarioslist);

                        if (!(instalacion.getComentarios() == null)) {
                            comentarios.addAll(instalacion.getComentarios());
                        }

                        activity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                if (comentarios.size() < 1) {
                                    recyclerView.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                } else {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    emptyView.setVisibility(View.GONE);
                                }
                            }
                        });


                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                activity.setListadecomentarios(comentarios);
                                activity.setComentarosAdapter(new CommentsFragmentRecyclerViewAdapter(activity.getListadecomentarios(), mListener));
                                recyclerView.setAdapter(activity.getComentarosAdapter());
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();
            activity.hiderogress();


        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ComentariosCallbackListener) {
            mListener = (ComentariosCallbackListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ComentariosCallbackListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void showDialogAddComment(Context context) {
        // custom dialog
        DialogFragment newFragment = new AddCommentDialogFragment();
        newFragment.show(getFragmentManager(), "add_comment");


    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }


}
