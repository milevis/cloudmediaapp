package geolocaliza.example.com.geolocaliza.adapters;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import geolocaliza.example.com.geolocaliza.R;

import geolocaliza.example.com.geolocaliza.TextView_Lato;
import geolocaliza.example.com.geolocaliza.listeners.SubtareasCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;


import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Subtarea} and makes a call to the
 * specified {@link SubtareasCallbackListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class SubtareasRecyclerViewAdapter extends RecyclerView.Adapter<SubtareasRecyclerViewAdapter.ViewHolder> {

    private List<Subtarea> mValues = new ArrayList<>();
    private final SubtareasCallbackListener mListener;

    public SubtareasRecyclerViewAdapter(ArrayList<Subtarea> items, SubtareasCallbackListener listener) {
         mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_subtareas, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(String.valueOf(mValues.get(position).getId()));
        holder.mContentView.setText(mValues.get(position).getDescription());

        if(!holder.mItem.isComplete()){
            holder.mContentView.setTextColor(Color.parseColor("#F68159"));
            holder.nocompletar.setVisibility(View.GONE);
        }else{
            holder.completar.setVisibility(View.GONE);
        }
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onClickSubTareaItem(holder.mItem);
                }
            }
        });

        holder.completar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.callbackClickComplete(holder.mItem, position);
                }
            }
        });
        holder.nocompletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.callbackClickUnComplete(holder.mItem, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mValues !=null){
            return mValues.size();
        }else {
            return 0;
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView_Lato completar;
        public final TextView_Lato nocompletar;
        public Subtarea mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            completar = (TextView_Lato) view.findViewById(R.id.completar);
            nocompletar = (TextView_Lato) view.findViewById(R.id.nocompletar);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
