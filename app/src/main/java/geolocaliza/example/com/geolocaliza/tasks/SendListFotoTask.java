package geolocaliza.example.com.geolocaliza.tasks;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import geolocaliza.example.com.geolocaliza.entity.Foto;
import geolocaliza.example.com.geolocaliza.listeners.FotosLCallbackLIstener;
import geolocaliza.example.com.geolocaliza.utils.Constants;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class SendListFotoTask extends AsyncTask<Double, Double, Boolean> {


    private int staffid;

    private int master_client;

    private ArrayList<Foto> fotostoSync = new ArrayList<Foto>();
    private ArrayList<Foto> fotosToDelete = new ArrayList<Foto>();

    FotosLCallbackLIstener callbackInterface;


    public SendListFotoTask(FotosLCallbackLIstener fotosLCallbackLIstener, ArrayList<Foto> fotostoSync, int staffid, int master_client) {
        super();
        this.callbackInterface = fotosLCallbackLIstener;
        this.fotostoSync = fotostoSync;
        this.staffid = staffid;
        this.master_client = master_client;


    }

    private String getPostDataString(JSONObject params) throws Exception {


        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        this.callbackInterface.callbackSyncFotos(result, fotosToDelete);
    }


    @Override
    protected Boolean doInBackground(Double... params) {
        StringBuffer response = new StringBuffer();
        try {

            for (Foto foto : fotostoSync) {
                URL url = new URL(Constants.API_URL+"addFileInstalacion/" + foto.getInstalacion_id());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);

                conn.setRequestMethod("POST");

                conn.setDoInput(true);
                conn.setDoOutput(true);
                JSONObject postDataParams = new JSONObject();
                postDataParams.put("fecha", foto.getFecha());
                postDataParams.put("file_name", foto.getName());
                postDataParams.put("file", foto.getImage_str_base_64());
                postDataParams.put("master_client", master_client);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));
                writer.flush();


                int responseCode = conn.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + getPostDataString(postDataParams));
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;


                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                System.out.println(response.toString());


                writer.close();
                os.close();

                conn.connect();

                if (wrapData(response)) {
                    fotosToDelete.add(foto);
                }

            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wrapData(response);
    }

    private boolean wrapData(StringBuffer response) {
        Boolean result = true;

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(response.toString()).getAsJsonObject();

            if (jsonObject.get("success").toString().isEmpty()) {
                result = false;
            }

        } catch (Exception e) {

            result = false;

        }

        return result;


    }


}