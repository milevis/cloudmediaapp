package geolocaliza.example.com.geolocaliza.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import geolocaliza.example.com.geolocaliza.entity.Foto;


@Dao
public interface FotoDao {

    @Query("SELECT * FROM foto")
    List<Foto> getAll();

    @Query("SELECT * FROM foto WHERE id IN (:ids)")
    List<Foto> loadAllByIds(int[] ids);


    @Query("SELECT * FROM foto WHERE id = (:id)")
    List<Foto>  loadAllById(int id);

    @Query("SELECT * FROM foto WHERE instalacion_id LIKE :instalacion_id")
    List<Foto> findAllByInstalacion(int instalacion_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Foto> fotos);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Foto foto);

    @Delete
    void delete(Foto foto);
}
