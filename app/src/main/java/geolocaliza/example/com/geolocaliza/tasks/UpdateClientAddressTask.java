package geolocaliza.example.com.geolocaliza.tasks;

import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.listeners.InstalationCallbackListener;
import geolocaliza.example.com.geolocaliza.utils.Constants;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class UpdateClientAddressTask extends AsyncTask<Double, Double, Boolean> {


    private int task;
    private double latitud;
    private double longitud;
    private String direccion;

    private ArrayList<Subtarea> subtareas = new ArrayList<Subtarea>();
    InstalationCallbackListener callbackInterface;

    public UpdateClientAddressTask(InstalationCallbackListener callbackInterface,int task, double latitud,
                                   double longitud, String  direccion) {

        super();
        this.latitud =latitud;
        this.task =task;
        this.longitud =longitud;
        this.direccion =direccion;
        this.callbackInterface = callbackInterface;
        // do stuff

        // do stuff
    }

    private String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        this.callbackInterface.callbackClickUpdateAddressClient(result);
    }


    @Override
    protected Boolean doInBackground(Double... params) {
        StringBuffer response = new StringBuffer();
        try {
            URL url = new URL(Constants.API_URL+"updateDireccionRef/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod("POST");

            conn.setDoInput(true);
            conn.setDoOutput(true);
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("task", task);
            postDataParams.put("longitud", longitud);
            postDataParams.put("latitud", latitud);
            postDataParams.put("direccion", direccion);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();


            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + getPostDataString(postDataParams));
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());


            writer.close();
            os.close();

            conn.connect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wrapData(response);
    }


    private boolean wrapData(StringBuffer response) {
       Boolean result = true;

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(response.toString()).getAsJsonObject();

            if( jsonObject.get("success").toString().isEmpty()){
                result = false;
            }




        }catch (Exception e){

            result = false;

        }

        return result;


    }

}