package geolocaliza.example.com.geolocaliza.listeners;

import android.graphics.Bitmap;
import android.net.Uri;

import java.text.ParseException;
import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.entity.Foto;

/**
 * Created by Jhainey on 02/06/2018.
 */

public interface FotosLCallbackLIstener {
    void onSelectFoto(Uri uri);
    void ongetFotos(ArrayList<Bitmap> fotos);
    void onUploadFoto(Boolean result, Bitmap bmp);
    void saveFotoOnDatabase(Uri uri, String photoName, Bitmap bmp, String image_str) throws ParseException;
    void callbackSyncFotos(Boolean result, ArrayList<Foto> fotos);
}
