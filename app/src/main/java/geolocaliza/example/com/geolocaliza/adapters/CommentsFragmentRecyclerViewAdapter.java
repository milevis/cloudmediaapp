package geolocaliza.example.com.geolocaliza.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import geolocaliza.example.com.geolocaliza.R;
import geolocaliza.example.com.geolocaliza.listeners.ComentariosCallbackListener;

import geolocaliza.example.com.geolocaliza.entity.Comentario;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Comentario} and makes a call to the
 * specified {@link ComentariosCallbackListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class CommentsFragmentRecyclerViewAdapter extends RecyclerView.Adapter<CommentsFragmentRecyclerViewAdapter.ViewHolder> {

    private final List<Comentario> mValues;
    private final ComentariosCallbackListener mListener;

    public CommentsFragmentRecyclerViewAdapter(List<Comentario> items, ComentariosCallbackListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(
                        R.layout.fragment_comments, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
      //  holder.mIdView.setText(String.valueOf(mValues.get(position).getId()));
        holder.mContentView.setText(mValues.get(position).getComentario());
        holder.staffname.setText(mValues.get(position).getStaffname());

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        try {
            Date date = format1.parse(mValues.get(position).getFecha_comentario());
            holder.fecha.setText(format1.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }



        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the acta  55rrrrrr16zxz5A D<NZ. ive callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onClickComentarioItem(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
      //  public final TextView mIdView;
        public final TextView mContentView;
        public final TextView staffname;
        public final TextView fecha;
        public Comentario mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            //mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            staffname = (TextView) view.findViewById(R.id.staffname);
            fecha = (TextView) view.findViewById(R.id.fecha);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
