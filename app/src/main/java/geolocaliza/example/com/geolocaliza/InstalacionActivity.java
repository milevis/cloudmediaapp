package geolocaliza.example.com.geolocaliza;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import geolocaliza.example.com.geolocaliza.adapters.CommentsFragmentRecyclerViewAdapter;
import geolocaliza.example.com.geolocaliza.adapters.FotosAdapter;
import geolocaliza.example.com.geolocaliza.adapters.SubtareasRecyclerViewAdapter;
import geolocaliza.example.com.geolocaliza.entity.Foto;
import geolocaliza.example.com.geolocaliza.fragments.CommentsFragment;
import geolocaliza.example.com.geolocaliza.fragments.FotosFragment;
import geolocaliza.example.com.geolocaliza.fragments.GeneralFragment;
import geolocaliza.example.com.geolocaliza.fragments.SubtareasFragment;
import geolocaliza.example.com.geolocaliza.listeners.ComentariosCallbackListener;
import geolocaliza.example.com.geolocaliza.listeners.FotosLCallbackLIstener;
import geolocaliza.example.com.geolocaliza.listeners.InstalationCallbackListener;
import geolocaliza.example.com.geolocaliza.listeners.InternetReceiverListener;
import geolocaliza.example.com.geolocaliza.listeners.SubtareasCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.entity.Staff;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.services.BootReceiverService;
import geolocaliza.example.com.geolocaliza.services.InternetConnectionReceiver;
import geolocaliza.example.com.geolocaliza.tasks.SendCommentTask;
import geolocaliza.example.com.geolocaliza.tasks.UpdateClientAddressTask;
import geolocaliza.example.com.geolocaliza.tasks.UpdateInstalacionTask;
import geolocaliza.example.com.geolocaliza.tasks.UpdateSubtareaTask;
import geolocaliza.example.com.geolocaliza.utils.AppDatabase;

public class InstalacionActivity extends AppCompatActivity implements InstalationCallbackListener,
        GeneralFragment.OnFragmentInteractionListener, SubtareasCallbackListener,
        ComentariosCallbackListener, FotosLCallbackLIstener, InternetReceiverListener, LocationListener {
    SharedPreferences sharedStaf;
    private Instalacion instalacion = new Instalacion();
    private Staff staff = new Staff();
    private Menu mOptionsMenu;

    private static final String DATABASE_NAME = "cloud_app";
    private ProgressDialog nDialog;
    private ArrayList<Bitmap> bitmapList = new ArrayList<>();
    AppDatabase database;
    FotosAdapter fotosAdapter;
    private CommentsFragmentRecyclerViewAdapter comentarosAdapter;
    private SubtareasRecyclerViewAdapter subtareasAdapter;

    private List<Comentario> listadecomentarios = new ArrayList<>();
    private List<Subtarea> listadesubtareas = new ArrayList<>();
    private boolean firstInternetCheck = false;
    private BootReceiverService serviceBootReceiver;
    private InternetConnectionReceiver internetConnectionReceiver;
    private TabLayout mTabLayout;
    private int[] mTabsIcons = {
            R.drawable.home,
            R.drawable.ic_format_list_checks_white_24dp,
            R.drawable.ic_comment_multiple_white_24dp,
            R.drawable.ic_camera_white_24dp};

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener

            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setTabGeneral();
                    return true;
                case R.id.navigation_dashboard:
                    setTabSubtareas();
                    return true;
                case R.id.navigation_notifications:
                    setTabComentarios();
                    return true;
                case R.id.navigation_fotos:
                    setFragmentFotos();
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instalacion);
        setTabesNav();


        nDialog = new ProgressDialog(InstalacionActivity.this);


        database = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).build();


        setSharedPreferences();
        Bundle b = getIntent().getExtras();
        instalacion.setId(b.getInt("instalacion_id"));


       /* BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);*/

        //  setTabGeneral();
        setReceivers();

    }

    private void setTabesNav() {

        try {
            ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
            MyPagerAdapter pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
            if (viewPager != null)
                viewPager.setAdapter(pagerAdapter);


            mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
            if (mTabLayout != null) {
                mTabLayout.setupWithViewPager(viewPager);

                for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                    TabLayout.Tab tab = mTabLayout.getTabAt(i);
                    if (tab != null)
                        tab.setCustomView(pagerAdapter.getTabView(i));
                }

                mTabLayout.getTabAt(0).getCustomView().setSelected(true);
            }
        } catch (Exception e) {
            Log.e("Instalation activity", e.getMessage());
        }

    }

    public void showProgress(String text) {
        nDialog.setMessage("Por favor espere..");
        nDialog.setTitle(text);
        nDialog.setIndeterminate(false);
        nDialog.setCancelable(true);
        nDialog.show();
    }

    public void hiderogress() {
        nDialog.dismiss();
    }


    private void setToolbarTitle(String title) {
        ActionBar toolbar = getSupportActionBar();

        toolbar.setTitle(title);
    }

    private void setTabGeneral() {

        //

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        GeneralFragment fragment = new GeneralFragment().newInstance(instalacion.getId(), staff.getId(), staff.getMaster_client());
        // fragmentTransaction.replace(R.id.instalacion_fragment_container, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commitAllowingStateLoss();

    }

    private void setTabSubtareas() {
        //  showProgress(this.getString(R.string.getting_info));
    //    setToolbarTitle("Subateras");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SubtareasFragment fragment = new SubtareasFragment();
        //   fragmentTransaction.replace(R.id.instalacion_fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    private void setTabComentarios() {
        //  showProgress(this.getString(R.string.getting_info));
       // setToolbarTitle("Comentarios");
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        CommentsFragment fragment = new CommentsFragment();
        //  fragmentTransaction.replace(R.id.instalacion_fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setFragmentFotos() {
        showProgress(this.getString(R.string.getting_info));

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FotosFragment fragment = new FotosFragment();
        // fragmentTransaction.replace(R.id.instalacion_fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void callbackGetInstalacion(Instalacion result) {
        instalacion = result;
        instalacion.setActualizao(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    database.instalacionDao().insertInstalacion(instalacion);
                    database.subtareaDao().insertAll(instalacion.getSubtareas());
                    database.comentarioDao().insertAll(instalacion.getComentarios());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        hideMenuItem();
        this.renderInstalacionData();
        this.hiderogress();

    }

    @Override
    public void callbackGetInstalaciones(ArrayList<Instalacion> result) {


    }

    @Override
    public void callbackClickInstalacion(Instalacion result, int position) {

    }

    @Override
    public void callbackClickUpdateInstalacionStatus(Boolean result, String status) {

        instalacion.setEstatus_plan(status);
        String text = "";
        if (result) {
            text = getString(R.string.status_updated);
        } else {
            text = getString(R.string.status_non_updated);
        }


        hiderogress();
        makeToast(text);
        hideMenuItem();
        this.renderInstalacionData();

    }

    @Override
    public void callbackClickUpdateAddressClient(Boolean result) {

        try {
            String text = "";
            if (result) {
                text = getString(R.string.addresss_updated);
            } else {
                text = getString(R.string.adress_non_updated);
            }

            hiderogress();
            makeToast(text);
            setTabGeneral();
        } catch (Exception e) {

        }

    }

    @Override
    public void callbackInternetConected(Boolean result) {

    }

    @Override
    public void callbackSIncronizeInstalaciones(Boolean subtarea, ArrayList<Instalacion> instalacions) {

    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public void onClickComentarioItem(Comentario item) {

    }

    @Override
    public void onListAddComment(final String comment) {

        final String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US).format(new Date());
        if (isNetworkAvailable()) {
            new SendCommentTask(this, comment, instalacion.getId(), staff.getId(), staff.getMaster_client(), date).execute();
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {


                    final Comentario comentario = new Comentario(comment, date.toString(), staff.getStaffname(), instalacion.getId(), staff.getId());
                    database.comentarioDao().insert(comentario);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String text = getString(R.string.comment_created);
                            makeToast(text);
                            setTabComentarios();

                        }
                    });


                }
            }).start();


        }
    }

    @Override
    public void callbackSendPostComment(Boolean result, ArrayList<Comentario> comentarios) {
        instalacion.setComentarios(comentarios);

        String text = "";
        if (result) {
            text = getString(R.string.comment_created);
            ;
        } else {
            text = getString(R.string.comment_non_created);
        }

        makeToast(text);
        hiderogress();

        refreshComments(comentarios);

    }

    private void refreshComments(ArrayList<Comentario> comentarios){


        CommentsFragment fragment = (CommentsFragment)
                getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.view_pager, 2));

        listadecomentarios = comentarios;
        synchronized (  comentarosAdapter) {

            comentarosAdapter.notifyDataSetChanged();
            comentarosAdapter.notifyAll();
        }
    }
    private void refreshSubtareas(ArrayList<Subtarea> subtareas){

        SubtareasFragment fragment = (SubtareasFragment)
                getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.view_pager, 1));

        listadesubtareas = subtareas;
        synchronized (  subtareasAdapter) {

            subtareasAdapter.notifyDataSetChanged();
            subtareasAdapter.notifyAll();
        }
    }

    @Override
    public void callbackSyncComents(Boolean result, ArrayList<Comentario> comentarios) {

    }

    public void renderInstalacionData() {

        try {

            GeneralFragment fragment = (GeneralFragment)
                    getSupportFragmentManager().findFragmentByTag(makeFragmentName(R.id.view_pager, 0));
// ...
            fragment.renderInstalacionData();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hiderogress();
        }

    }

    public Instalacion getInstalacion() {
        return instalacion;
    }

    public void setInstalacion(Instalacion instalacion) {
        this.instalacion = instalacion;
    }

    private void setSharedPreferences() {

        sharedStaf = getSharedPreferences("STAFF", 0);
        staff.setStaffname(sharedStaf.getString("NAME", ""));
        staff.setEmail(sharedStaf.getString("EMAIL", ""));
        staff.setId(sharedStaf.getInt("ID", 0));
        staff.setMaster_client(sharedStaf.getInt("MASTER_CLIENT", 0));
    }


    @Override
    public void onBackPressed() {
        Intent mainIntent = new Intent().setClass(InstalacionActivity.this, MainActivity.class);
        startActivity(mainIntent);
    }

    @Override
    public void onClickSubTareaItem(Subtarea item) {

    }

    @Override
    public void callbackUpdateSubtask(Boolean result, ArrayList<Subtarea> subtareas) {


        String text = "";
        if (result) {
            instalacion.setSubtareas(subtareas);
            text = getString(R.string.subtarea_updated);
        } else {
            text = getString(R.string.subtarea_non_updated);
        }


        hiderogress();
        makeToast(text);
        refreshSubtareas(subtareas);

    }

    private static String makeFragmentName(int viewPagerId, int index) {
        return "android:switcher:" + viewPagerId + ":" + index;
    }

    @Override
    public void callbackClickComplete(Subtarea subtarea, int position) {

        showProgress(getString(R.string.updating_subtarea));
        if (isNetworkAvailable()) {
            new UpdateSubtareaTask(this, 1, instalacion.getId(), staff.getId(), staff.getMaster_client(), subtarea.getId()).execute();
        } else {
            subtarea.setComplete(true);
            updateSubtaskOnDatabase(subtarea, position);
        }

    }

    @Override
    public void callbackClickUnComplete(Subtarea subtarea, int position) {
        showProgress(getString(R.string.updating_subtarea));
        if (isNetworkAvailable()) {
            new UpdateSubtareaTask(this, 0, instalacion.getId(), staff.getId(), staff.getMaster_client(), subtarea.getId()).execute();
        } else {
            subtarea.setComplete(false);
            updateSubtaskOnDatabase(subtarea, position);
        }
    }

    @Override
    public void callbackSIncronizeSubtasks(Boolean subtarea, ArrayList<Subtarea> subtareasudates) {

    }

    private void setReceivers() {

        serviceBootReceiver = new BootReceiverService();
        internetConnectionReceiver = new InternetConnectionReceiver(this);
        //en vesiones nuevas no hace falta , asta cn el manifiest
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

        registerReceiver(internetConnectionReceiver, filter);


        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction("ac.in.ActivityRecognition.RestartSensor");
        registerReceiver(serviceBootReceiver, screenStateFilter);

    }

    private void updateSubtaskOnDatabase(final Subtarea subtarea, final int position) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    database.subtareaDao().updateSubtarea(subtarea);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            // instalacion.getSubtareas().remove(position);
                            //instalacion.getSubtareas().add(subtarea);
                            hiderogress();
                            makeToast("Subtrea actualizada");
                            refreshSubtareas((ArrayList<Subtarea>) instalacion.getSubtareas());

                        }
                    });

                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    hiderogress();
                }
            }
        }).start();

    }

    private void updateInstalacionOnDatabase(final Instalacion instalacion) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    database.instalacionDao().updateInstalacion(instalacion);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            hiderogress();
                            makeToast("Instalacion actualizada");
                            hiderogress();
                            hideMenuItem();
                            renderInstalacionData();

                        }
                    });

                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    hiderogress();
                }
            }
        }).start();

    }

    /**********************Menu topbar****************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.top_nav_instalacion, menu);

        mOptionsMenu = menu;
        hideMenuItem();
        return true;
    }

    private void hideMenuItem() {

        try {
            MenuItem instal_action = mOptionsMenu.findItem(R.id.instal_action);
            MenuItem cancel_action = mOptionsMenu.findItem(R.id.cancel_action);
            if (instalacion.getEstatus_plan() != null) {
                if (!instalacion.getEstatus_plan().isEmpty()) {
                    if (instalacion.getEstatus_plan().equals("INSTALADO")) {
                        instal_action.setVisible(false);
                        cancel_action.setVisible(true);
                    } else {
                        instal_action.setVisible(true);
                        cancel_action.setVisible(false);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("Instalacion activity", e.getMessage());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cancel_action) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
// Add the buttons

            builder.setTitle(R.string.dialog_cancel_install);
            final InstalationCallbackListener callbackListener = this;
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    showProgress(getString(R.string.updating_instalation));
                    if (isNetworkAvailable()) {
                        new UpdateInstalacionTask(callbackListener, "CANCELADO", instalacion.getId(), staff.getId(), staff.getMaster_client()).execute();
                    } else {
                        instalacion.setEstatus_plan("CANCELADO");
                        updateInstalacionOnDatabase(instalacion);
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    dialog.dismiss();
                }
            });

            builder.show();
        }

        if (id == R.id.action_router) {
            configurarRouter();


        }


        if (id == R.id.instal_action) {


            AlertDialog.Builder builder = new AlertDialog.Builder(this);
// Add the buttons
            builder.setTitle(R.string.dialog_install_install);
            final InstalationCallbackListener callbackListener = this;
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    showProgress(getString(R.string.updating_instalation));
                    if (isNetworkAvailable()) {
                        instalacion.setEstatus_plan("INSTALADO");
                        new UpdateInstalacionTask(callbackListener, "INSTALADO", instalacion.getId(), staff.getId(), staff.getMaster_client()).execute();
                    } else {
                        updateInstalacionOnDatabase(instalacion);
                    }
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    dialog.dismiss();
                }
            });

            builder.show();


        }

        if (id == R.id.action_map) {


            Intent mainIntent = new Intent().setClass(InstalacionActivity.this, MapsActivity.class);
            mainIntent.putExtra("latitud", instalacion.getLatitud());
            mainIntent.putExtra("longitud", instalacion.getLogitud());
            startActivity(mainIntent);
        }

        /*if (id == R.id.action_pictures) {
            setFragmentFotos();
        }*/

        if (id == R.id.update_adres) {
            try {
                clientAddresUpdate();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void configurarRouter() {
        Intent mainIntent = new Intent().setClass(InstalacionActivity.this, WebViewActivity.class);
        startActivity(mainIntent);

    }

    private void clientAddresUpdate() throws IOException {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            double latitude=0;
            double longitude=0;
            try {


                LocationManager locationManager = (LocationManager) getApplicationContext()
                        .getSystemService(LOCATION_SERVICE);

                // getting GPS status
                Boolean isGPSEnabled = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);

                // getting network status
                Boolean isNetworkEnabled = locationManager
                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (!isGPSEnabled && !isNetworkEnabled) {
                    // no network provider is enabled
                    makeToast("GPS o Internet no disponible");
                } else {

                    Location location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                10,
                                10, this);
                        Log.d("Network", "Network Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    2,
                                    2, this);
                            Log.d("GPS", "GPS Enabled");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();
                                }
                            }
                        }
                    }
                }




                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                String direccion = addresses.get(0).getAddressLine(0);
                // direccion += " "+addresses.get(0).getLocality();

                showProgress(getString(R.string.updatting_address));
                new UpdateClientAddressTask(this, instalacion.getId(), latitude, longitude, direccion).execute();
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                makeToast("Ha ocurrido un error al actualizar la dirección del cliente");
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    2);
        }
    }

    @Override
    public void onSelectFoto(Uri uri) {

    }

    @Override
    public void ongetFotos(ArrayList<Bitmap> fotos) {

        instalacion.setImagenes(fotos);
        setFragmentFotos();
    }

    @Override
    public void onUploadFoto(Boolean result, final Bitmap bmp) {


        hiderogress();
        String text = "";
        if (result) {

            getBitmapList().add(bmp);

            synchronized (fotosAdapter) {
                fotosAdapter.notifyDataSetChanged();
                fotosAdapter.notifyAll();
            }

            text = "La foto se ha subido al servidor";
        } else {
            text = "No se ha podido subir la foto al servidor";
        }


        makeToast(text);
    }

    @Override
    public void saveFotoOnDatabase(final Uri uri, String photoName, final Bitmap bmp, String
            image_str) {
        final String path = uri.getPath();
        final String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US).format(new Date());
        final Foto foto = new Foto(path, instalacion.getId(), date, photoName, image_str);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    database.fotoDao().insert(foto);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            getBitmapList().add(bmp);
                            synchronized (fotosAdapter) {
                                fotosAdapter.notifyDataSetChanged();
                                fotosAdapter.notifyAll();
                            }
                            hiderogress();

                        }
                    });

                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    hiderogress();
                }
            }
        }).start();


    }

    @Override
    public void callbackSyncFotos(Boolean result, ArrayList<Foto> fotos) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    public Staff getStaff() {
        return staff;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    // startServiceCron();
                    try {
                        clientAddresUpdate();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.


                    makeToast("No tiene Gps Habilitado, imposible compartir ubicación!");


                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    public void makeToast(String textto) {
        Context context = getApplicationContext();
        CharSequence text = textto;
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public AppDatabase getDatabase() {
        return database;
    }

    public void setDatabase(AppDatabase database) {
        this.database = database;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public String getRealPathFromURI(Uri contentUri) {
        String path = null;
        String[] proj = {MediaStore.MediaColumns.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        path = cursor.getString(column_index);
        cursor.close();
        return path;
    }


    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private String getStringUriFromBmp(Context context, Bitmap inImage) {


        Uri uri = getImageUri(context, inImage);
        return getRealPathFromURI(uri);
    }

    public Uri stringToUri(String s) {
        Uri mUri = Uri.parse(s);
        return mUri;
    }

    public Bitmap uriToBItmap(Uri uri) throws FileNotFoundException, IOException {
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();
        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        // double ratio = (originalSize > THUMBNAIL_SIZE) ? (originalSize / THUMBNAIL_SIZE) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        // bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    public ArrayList<Bitmap> getBitmapList() {
        return bitmapList;
    }

    public void setBitmapList(ArrayList<Bitmap> bitmapList) {
        this.bitmapList = bitmapList;
    }

    public FotosAdapter getFotosAdapter() {
        return fotosAdapter;
    }

    public void setFotosAdapter(FotosAdapter fotosAdapter) {
        this.fotosAdapter = fotosAdapter;
    }

    public Bitmap scaeImage(Bitmap bmp) {

        int nh = (int) (bmp.getHeight() * (512.0 / bmp.getWidth()));
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, 512, nh, true);
        return scaledBitmap;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(serviceBootReceiver);
            unregisterReceiver(internetConnectionReceiver);
        } catch (Exception e) {
            Log.e("main activity", e.getMessage());
        }
    }

    @Override
    public void haveInternetListener() {
        //makeToast(getString(R.string.internet));
    }

    @Override
    public void dontHaveInternetListener() {
        //   makeToast(getString(R.string.no_internet));
    }

    public boolean isFirstInternetCheck() {
        return firstInternetCheck;
    }

    public void setFirstInternetCheck(boolean firstInternetCheck) {
        this.firstInternetCheck = firstInternetCheck;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public final int PAGE_COUNT = 4;

        private final String[] mTabsTitle = {"General", "Subtareas", "Comentarios", "Fotos"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
            View view = LayoutInflater.from(InstalacionActivity.this).inflate(R.layout.custom_tab, null);
//            TextView title = (TextView) view.findViewById(R.id.title);
//            title.setText(mTabsTitle[position]);
            ImageView icon = (ImageView) view.findViewById(R.id.icon);
            icon.setImageResource(mTabsIcons[position]);
            return view;
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
                 //   setToolbarTitle("General");
                    return new GeneralFragment().newInstance(instalacion.getId(), staff.getId(), staff.getMaster_client());
                case 1:
                   // setToolbarTitle("Subtareas");
                    return new SubtareasFragment().newInstance(instalacion.getSubtareas() == null ? 0 : instalacion.getSubtareas().size());
                case 2:
                    //setToolbarTitle("Comentarios");
                    return new CommentsFragment().newInstance(instalacion.getComentarios() == null ? 0 : instalacion.getSubtareas().size());
                case 3:
                  //  setToolbarTitle("Fotos");
                    return new FotosFragment().newInstance(0);
            }
            return null;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mTabsTitle[position];
        }
    }

    public CommentsFragmentRecyclerViewAdapter getComentarosAdapter() {
        return comentarosAdapter;
    }

    public void setComentarosAdapter(CommentsFragmentRecyclerViewAdapter comentarosAdapter) {
        this.comentarosAdapter = comentarosAdapter;
    }

    public List<Comentario> getListadecomentarios() {
        return listadecomentarios;
    }

    public void setListadecomentarios(List<Comentario> listadecomentarios) {
        this.listadecomentarios = listadecomentarios;
    }

    public SubtareasRecyclerViewAdapter getSubtareasAdapter() {
        return subtareasAdapter;
    }

    public void setSubtareasAdapter(SubtareasRecyclerViewAdapter subtareasAdapter) {
        this.subtareasAdapter = subtareasAdapter;
    }

    public List<Subtarea> getListadesubtareas() {
        return listadesubtareas;
    }

    public void setListadesubtareas(List<Subtarea> listadesubtareas) {
        this.listadesubtareas = listadesubtareas;
    }
}

