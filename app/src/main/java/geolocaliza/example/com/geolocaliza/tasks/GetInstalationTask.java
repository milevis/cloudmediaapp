package geolocaliza.example.com.geolocaliza.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.listeners.InstalationCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.utils.Constants;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class GetInstalationTask extends AsyncTask<Double, Double, Instalacion> {


    InstalationCallbackListener callbackInterface;

    int instalacion_id;
    int user_id;
    int master_client;
    public GetInstalationTask(InstalationCallbackListener callbackInterface, int instalacionId, int user_id, int master_client) {

        super();
        this.callbackInterface = callbackInterface;
        this.instalacion_id = instalacionId;
        this.user_id = user_id;
        this.master_client = master_client;
        // do stuff
    }

    @Override
    protected void onPostExecute(Instalacion result) {
        super.onPostExecute(result);
        this.callbackInterface.callbackGetInstalacion(result);
    }


    @Override
    protected Instalacion doInBackground(Double... params) {

        StringBuffer response = new StringBuffer();

        try {
            URL url = new URL(Constants.API_URL+"installTask/"+instalacion_id+"/"+user_id+"/"+master_client);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod("GET");

            conn.setDoInput(true);
            conn.setDoOutput(true);


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.flush();


            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'get' request to URL : " + url);

            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;


            int i = 0;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
                i++;
            }
            in.close();

            System.out.println(response.toString());


            writer.close();
            os.close();

            conn.connect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wrapData(response);
    }

    private Instalacion wrapData(StringBuffer response) {
        Instalacion instalacion = new Instalacion();
        try {

            ArrayList<Bitmap> fotos = new ArrayList<>();
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(response.toString()).getAsJsonObject();

            JsonObject task = (JsonObject) jsonObject.get("task");

            JsonElement direccion = task.get("direccionapp");
            int instalacion_id =Integer.parseInt(task.get("planes_instal_serv_id").toString().replace("\"", ""));
            instalacion.setId(instalacion_id);
            instalacion.setEstatus_plan(task.get("estatus_plan").toString().replace("\"", ""));
            if(direccion!=null) {
                instalacion.setDireccion(direccion.toString().replace("\"", ""));
            }
            instalacion.setNameSurname(task.get("namesurname").toString().replace("\"", ""));
            instalacion.setNombre_servicio(task.get("nombre_servicio").toString().replace("\"", ""));
            instalacion.setApellidoResidencial(task.get("apellido_residencial").toString().replace("\"", ""));
            instalacion.setCompany(task.get("company").toString().replace("\"", ""));
            instalacion.setFechaCreacion(task.get("creado").toString().replace("\"", ""));
            instalacion.setFecha_instalacion(task.get("fecha_instalacion").toString().replace("\"", ""));
            instalacion.setFechaInstalado(task.get("fecha_instalado").toString().replace("\"", ""));
            instalacion.setType(Integer.parseInt(task.get("type").toString().replace("\"", "")));

            instalacion.setLatitud(Double.parseDouble(task.get("latitud_referencia").toString().replace("\"", "")));
            instalacion.setLogitud(Double.parseDouble(task.get("longitud_referencia").toString().replace("\"", "")));


            //instalacion.setPlan_id(Integer.parseInt(task.get("plan_id").toString().replace("\"", "")));


            //Wrapeo los comentarios
            ArrayList<Comentario> comentarios = new ArrayList<>();
            for (JsonElement comentarioJson :
                    jsonObject.get("comentarios").getAsJsonArray()) {
                String comentarioDescription = comentarioJson.getAsJsonObject().get("comentario").toString().replace("\"", "");
                String staffname = comentarioJson.getAsJsonObject().get("staffname").toString().replace("\"", "");
                int id = Integer.parseInt(comentarioJson.getAsJsonObject().get("id").toString().replace("\"", ""));
                String fecha = comentarioJson.getAsJsonObject().get("fecha_comentario").toString().replace("\"", "");
                Comentario comentario = new Comentario(id
                        , comentarioDescription, fecha, staffname, instalacion_id);

                comentarios.add(comentario);
            }
            instalacion.setComentarios(comentarios);

            //Wrapeo los comentarios
            ArrayList<Subtarea> subtareas = new ArrayList<>();
            for (JsonElement subtareaJson :
                    jsonObject.get("subtareas").getAsJsonArray()) {
                String description = subtareaJson.getAsJsonObject().get("description").toString().replace("\"", "");
                int id = Integer.parseInt(subtareaJson.getAsJsonObject().get("id").toString().replace("\"", ""));
                int completeInt = Integer.valueOf(subtareaJson.getAsJsonObject().get("complete").toString().replace("\"", ""));
                int finishedInt = Integer.valueOf(subtareaJson.getAsJsonObject().get("finished").toString().replace("\"", ""));
                boolean finished = finishedInt==1?true:false;
                boolean complete = completeInt==1?true:false;
                Subtarea subtarea = new Subtarea(id
                        ,description, finished, complete, instalacion_id);

                subtareas.add(subtarea);
            }
            instalacion.setSubtareas(subtareas);


            for (JsonElement fotoJson :
                    jsonObject.get("files").getAsJsonArray()) {
                try {
                    String name = fotoJson.getAsJsonObject().get("name").toString().replace("\"", "");
                    Bitmap bitmap = urlImageToBitmap(Constants.NORMAL_URL+"uploads/files/"+name);

                    int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    instalacion.getImagenes().add(scaled);

                }catch (Exception e){
                    Log.e("error leyendo la imagen", e.getMessage());
                }

            }


        }catch (Exception e){

            Log.e("Error", e.getMessage());
        }

        return instalacion;


    }

    private Bitmap urlImageToBitmap(String imageUrl) throws Exception {
        Bitmap result = null;
        URL url = new URL(imageUrl);
        if(url != null) {
            result = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        }
        return result;
    }

}