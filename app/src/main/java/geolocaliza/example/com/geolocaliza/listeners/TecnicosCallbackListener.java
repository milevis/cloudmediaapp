package geolocaliza.example.com.geolocaliza.listeners;

import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.entity.Staff;

/**
 * Created by Jhainey on 20/05/2018.
 */

public interface TecnicosCallbackListener {

    public void getTecnicosCallback(ArrayList<Staff>  result);
}
