package geolocaliza.example.com.geolocaliza.entity;

import android.arch.persistence.room.*;
import android.graphics.Bitmap;
import android.view.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jhainey on 30/05/2018.
 */

@Entity
public class Instalacion {
    @PrimaryKey
    private int id;
    private int servicioinstalacion_id;
    private int plan_id;
    private String nombre_servicio;
    private int customer_id;
    private double costo_instalacion;
    private int nro_contrato;
    private String prioridad;
    private int factura_fisica;
    private String tipo_instalacion;
    private String fecha_instalacion;
    private String observacion;
    private boolean actualizao =false;

    private String estatus_plan;

    private int orden_id;
    @Ignore
    private List<String> imagesUri;

    @Ignore
    private List<Comentario> comentarios;
    @Ignore
    private List<Subtarea> subtareas;
    @Ignore
    private ArrayList<Bitmap> imagenes =  new ArrayList<Bitmap>();


    public boolean isActualizao() {
        return actualizao;
    }

    public void setActualizao(boolean actualizao) {
        this.actualizao = actualizao;
    }

    public List<String> getImagesUri() {
        return imagesUri;
    }

    public void setImagesUri(List<String> imagesUri) {
        this.imagesUri = imagesUri;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public void setSubtareas(List<Subtarea> subtareas) {
        this.subtareas = subtareas;
    }

    private double latitud;
    private double logitud;
    private String direccion;
    private String nameSurname;
    private String apellidoResidencial;
    private String company;
    private String fechaCreacion;
    private String fechaInstalacion;
    private String fechaInstalado;
    private String tiempoInstalacion;
    private int type;

    public ArrayList<Bitmap> getImagenes() {
        return imagenes;
    }

    public void setImagenes(ArrayList<Bitmap> imagenes) {
        this.imagenes = imagenes;
    }

    public Instalacion() {
    }


    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLogitud() {
        return logitud;
    }

    public void setLogitud(double logitud) {
        this.logitud = logitud;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getApellidoResidencial() {
        return apellidoResidencial;
    }

    public void setApellidoResidencial(String apellidoResidencial) {
        this.apellidoResidencial = apellidoResidencial;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(String fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public String getFechaInstalado() {
        return fechaInstalado;
    }

    public void setFechaInstalado(String fechaInstalado) {
        this.fechaInstalado = fechaInstalado;
    }

    public String getTiempoInstalacion() {
        return tiempoInstalacion;
    }

    public void setTiempoInstalacion(String tiempoInstalacion) {
        this.tiempoInstalacion = tiempoInstalacion;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getEstatus_plan() {
        return estatus_plan;
    }

    public void setEstatus_plan(String estatus_plan) {
        this.estatus_plan = estatus_plan;
    }

    public String getNombre_servicio() {
        return nombre_servicio;
    }

    public void setNombre_servicio(String nombre_servicio) {
        this.nombre_servicio = nombre_servicio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getServicioinstalacion_id() {
        return servicioinstalacion_id;
    }

    public void setServicioinstalacion_id(int servicioinstalacion_id) {
        this.servicioinstalacion_id = servicioinstalacion_id;
    }

    public int getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(int plan_id) {
        this.plan_id = plan_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public double getCosto_instalacion() {
        return costo_instalacion;
    }

    public void setCosto_instalacion(double costo_instalacion) {
        this.costo_instalacion = costo_instalacion;
    }

    public int getNro_contrato() {
        return nro_contrato;
    }

    public void setNro_contrato(int nro_contrato) {
        this.nro_contrato = nro_contrato;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public int getFactura_fisica() {
        return factura_fisica;
    }

    public void setFactura_fisica(int factura_fisica) {
        this.factura_fisica = factura_fisica;
    }

    public String getTipo_instalacion() {
        return tipo_instalacion;
    }

    public void setTipo_instalacion(String tipo_instalacion) {
        this.tipo_instalacion = tipo_instalacion;
    }

    public String getFecha_instalacion() {
        return fecha_instalacion;
    }

    public void setFecha_instalacion(String fecha_instalacion) {
        this.fecha_instalacion = fecha_instalacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getOrden_id() {
        return orden_id;
    }

    public void setOrden_id(int orden_id) {
        this.orden_id = orden_id;
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public List<Subtarea> getSubtareas() {
        return subtareas;
    }

    public void setSubtareas(ArrayList<Subtarea> subtareas) {
        this.subtareas = subtareas;
    }


}
