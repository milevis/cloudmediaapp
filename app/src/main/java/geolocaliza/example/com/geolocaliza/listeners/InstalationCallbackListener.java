package geolocaliza.example.com.geolocaliza.listeners;

import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.entity.Instalacion;

/**
 * Created by Jhainey on 20/05/2018.
 */

public interface InstalationCallbackListener {

    public void callbackGetInstalacion(Instalacion result);
    public void callbackGetInstalaciones(ArrayList<Instalacion> result);
    public void callbackClickInstalacion(Instalacion result, int position);
    public void callbackClickUpdateInstalacionStatus(Boolean result, String estatus);
    public void callbackClickUpdateAddressClient(Boolean result);
    public void callbackInternetConected(Boolean result);
    public void callbackSIncronizeInstalaciones(Boolean subtarea, ArrayList<Instalacion> instalacions);
}
