package geolocaliza.example.com.geolocaliza.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import geolocaliza.example.com.geolocaliza.entity.Comentario;


@Dao
public interface ComentarioDao {

    @Query("SELECT * FROM comentario")
    List<Comentario> getAll();

    @Query("SELECT * FROM comentario WHERE id IN (:ids)")
    List<Comentario> loadAllByIds(int[] ids);


    @Query("SELECT * FROM comentario WHERE id = (:id)")
    Comentario loadAllById(int id);

    @Query("SELECT * FROM comentario WHERE instalacion_id LIKE :instalacion_id")
    List<Comentario> findAllByInstalacion(int instalacion_id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Comentario> comentarios);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Comentario comentario);

    @Delete
    void delete(Comentario comentario);
}
