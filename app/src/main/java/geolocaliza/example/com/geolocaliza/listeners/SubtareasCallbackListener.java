package geolocaliza.example.com.geolocaliza.listeners;

import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.entity.Subtarea;

/**
 * Created by Jhainey on 02/06/2018.
 */

public interface SubtareasCallbackListener {
    void onClickSubTareaItem(Subtarea item);
    void callbackUpdateSubtask(Boolean result, ArrayList<Subtarea> subtareas);
    void callbackClickComplete(Subtarea subtarea, int position);
    void callbackClickUnComplete(Subtarea subtarea, int position);
    void callbackSIncronizeSubtasks(Boolean subtarea, ArrayList<Subtarea> subtareasudates);
}
