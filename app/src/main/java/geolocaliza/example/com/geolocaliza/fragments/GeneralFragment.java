package geolocaliza.example.com.geolocaliza.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.InstalacionActivity;
import geolocaliza.example.com.geolocaliza.R;
import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.listeners.InstalationCallbackListener;
import geolocaliza.example.com.geolocaliza.tasks.GetInstalationTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GeneralFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GeneralFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GeneralFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "instalacion_id";
    private static final String ARG_PARAM2 = "user_id";
    private static final String ARG_PARAM3 = "master_client";
    InstalacionActivity activity = (InstalacionActivity) getActivity();
    private View rootView;
    // TODO: Rename and change types of parameters
    private int instalacion_id;
    private int user_id;
    private int master_client;
    private Instalacion activityInstalacion;
    private View view;

    private OnFragmentInteractionListener mListener;

    public GeneralFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param instalacion_id
     * @param user_id
     * @return A new instance of fragment GeneralFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GeneralFragment newInstance(int instalacion_id, int user_id, int master_client) {
        GeneralFragment fragment = new GeneralFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, instalacion_id);
        args.putInt(ARG_PARAM2, user_id);
        args.putInt(ARG_PARAM3, master_client);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            instalacion_id = getArguments().getInt(ARG_PARAM1);
            user_id = getArguments().getInt(ARG_PARAM2);
            master_client = getArguments().getInt(ARG_PARAM3);
        }

        activity = (InstalacionActivity) getActivity();
        if (isNetworkAvailable()) {

            if (activity.getInstalacion().isActualizao()) {
                // activity.renderInstalacionData();
            } else {
                activity.showProgress(this.getString(R.string.getting_info));
                new GetInstalationTask((InstalationCallbackListener) getActivity(), instalacion_id, user_id, master_client).execute();
            }


        }
        if (!isNetworkAvailable()) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Instalacion instalacion = activity.getDatabase().instalacionDao().loadById(instalacion_id);
                    // ArrayList<Subtarea> subtareas = (ArrayList<Subtarea>) activity.getDatabase().subtareaDao().findAllByInstalacion(instalacion_id);
                    // ArrayList<Comentario> comentarios = (ArrayList<Comentario>) activity.getDatabase().comentarioDao().findAllByInstalacion(instalacion_id);
                    // instalacion.setSubtareas(subtareas);
                    //instalacion.setComentarios(comentarios);
                    activity.setInstalacion(instalacion);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.renderInstalacionData();
                        }
                    });
                }
            }).start();
        }


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_general, container, false);
//TODO ME QUEDE AQUI
        if (isNetworkAvailable()) {

            if (activity.getInstalacion().isActualizao()) {
                renderInstalacionData();
            }


        }

        return view;

    }


    public void renderInstalacionData() {


        try {
            TextView id = (TextView) view.findViewById(R.id.id);
            TextView cliente = (TextView) view.findViewById(R.id.cliente);
            TextView ubicacion = (TextView) view.findViewById(R.id.ubicacion);
            TextView ubicacion_titulo = (TextView) view.findViewById(R.id.ubicacion_titulo);
            TextView creado = (TextView) view.findViewById(R.id.creado);
            TextView nombre = (TextView) view.findViewById(R.id.nombre);
            TextView feha_instalacion = (TextView) view.findViewById(R.id.feha_instalacion);
            TextView fecha_instalado_titulo = (TextView) view.findViewById(R.id.fecha_instalado_titulo);
            TextView prioridad = (TextView) view.findViewById(R.id.prioridad);
            TextView prioridad_titulo = (TextView) view.findViewById(R.id.prioridad_titulo);
            TextView tiempo_instalacion_titulo = (TextView) view.findViewById(R.id.tiempo_instalacion_titulo);

            TextView estado = (TextView) view.findViewById(R.id.estado);
            TextView fecha_instalado = (TextView) view.findViewById(R.id.fecha_instalado);
            TextView tiempo_instalacion = (TextView) view.findViewById(R.id.tiempo_instalacion);

            id.setText(String.valueOf(activity.getInstalacion().getId()));

            if (activity.getInstalacion().getType() == 0) {
                //cliente.setText(String.valueOf(instalacion.getNameSurname())); TODO DESCOMENTAR CUANDO FERNANDO ARREGLE EL API
                cliente.setText(String.valueOf(activity.getInstalacion().getNameSurname()));
            } else {
                cliente.setText(String.valueOf(activity.getInstalacion().getNameSurname() + " " + activity.getInstalacion().getApellidoResidencial()));
            }
            if(activity.getInstalacion().getDireccion()!=null) {

                ubicacion.setText(String.valueOf(activity.getInstalacion().getDireccion()));
            }else{
                ubicacion.setVisibility(View.GONE);
                ubicacion_titulo.setVisibility(View.GONE);
            }
            creado.setText(String.valueOf(activity.getInstalacion().getFechaCreacion()));
            nombre.setText(String.valueOf(activity.getInstalacion().getNombre_servicio()));
            feha_instalacion.setText(String.valueOf(activity.getInstalacion().getFecha_instalacion()));

            if(activity.getInstalacion().getPrioridad()!=null) {
                prioridad.setText(String.valueOf(activity.getInstalacion().getPrioridad()));

            }else{
                prioridad.setVisibility(View.GONE);
                prioridad_titulo.setVisibility(View.GONE);
            }

            estado.setText(String.valueOf(activity.getInstalacion().getEstatus_plan()));

            if(activity.getInstalacion().getFechaInstalado()!=null){
                fecha_instalado.setText(String.valueOf(activity.getInstalacion().getFechaInstalado()));

            }else{
                fecha_instalado.setVisibility(View.GONE);
                fecha_instalado_titulo.setVisibility(View.GONE);
            }

            if(activity.getInstalacion().getTiempoInstalacion()!=null) {

                tiempo_instalacion.setText(String.valueOf(activity.getInstalacion().getTiempoInstalacion()));
            }else{
                tiempo_instalacion_titulo.setVisibility(View.GONE);
                tiempo_instalacion.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            activity.hiderogress();
            //activity.hideMenuItem();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {


            mListener = (OnFragmentInteractionListener) context;


        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
