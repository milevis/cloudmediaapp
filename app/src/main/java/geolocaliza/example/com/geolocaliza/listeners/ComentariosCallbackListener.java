package geolocaliza.example.com.geolocaliza.listeners;

import java.text.ParseException;
import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.entity.Comentario;

/**
 * Created by Jhainey on 02/06/2018.
 */

public interface ComentariosCallbackListener {
    void onClickComentarioItem(Comentario item);
    void onListAddComment(String comment) throws ParseException;
    void callbackSendPostComment(Boolean result, ArrayList<Comentario> comentarios);
    void callbackSyncComents(Boolean result, ArrayList<Comentario> comentarios);
}
