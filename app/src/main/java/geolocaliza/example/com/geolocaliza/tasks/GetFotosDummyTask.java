package geolocaliza.example.com.geolocaliza.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.net.URL;
import java.util.ArrayList;

import geolocaliza.example.com.geolocaliza.listeners.FotosLCallbackLIstener;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class GetFotosDummyTask extends AsyncTask<Double, Double,  ArrayList<Bitmap>> {

    private String email;
    private Exception exception;
    private ArrayList<Bitmap> bitmapList = new ArrayList<Bitmap>();
    FotosLCallbackLIstener callbackInterface;

    public GetFotosDummyTask(FotosLCallbackLIstener callbackInterface) {

        super();
        this.callbackInterface = callbackInterface;
        // do stuff
    }

    @Override
    protected void onPostExecute( ArrayList<Bitmap> result) {
        super.onPostExecute(result);
        this.callbackInterface.ongetFotos(result);
    }



    @Override
    protected  ArrayList<Bitmap> doInBackground(Double... params) {
        try {
            for(int i = 0; i < 10; i++) {
                Bitmap bitmap = urlImageToBitmap("http://placehold.it/150x150");
                bitmapList.add(bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return this.bitmapList;
    }

    private Bitmap urlImageToBitmap(String imageUrl) throws Exception {
        Bitmap result = null;
        URL url = new URL(imageUrl);
        if(url != null) {
            result = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        }
        return result;
    }

}