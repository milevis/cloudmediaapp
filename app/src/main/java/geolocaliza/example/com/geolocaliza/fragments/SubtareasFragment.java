package geolocaliza.example.com.geolocaliza.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import geolocaliza.example.com.geolocaliza.InstalacionActivity;
import geolocaliza.example.com.geolocaliza.R;
import geolocaliza.example.com.geolocaliza.adapters.CommentsFragmentRecyclerViewAdapter;
import geolocaliza.example.com.geolocaliza.adapters.SubtareasRecyclerViewAdapter;
import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.listeners.SubtareasCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link SubtareasCallbackListener}
 * interface.
 */
public class SubtareasFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private SubtareasCallbackListener mListener;
    private TextView emptyView;
    private List<Subtarea> subtareas = new ArrayList<Subtarea>();
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SubtareasFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static SubtareasFragment newInstance(int columnCount) {
        SubtareasFragment fragment = new SubtareasFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subtareas_list, container, false);
        // Set the adapter
        this.emptyView = (TextView) view.findViewById(R.id.empty_view);
        Context context = view.getContext();
        final Instalacion instalacion = ((InstalacionActivity) context).getInstalacion();
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        ;
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        final InstalacionActivity activity = (InstalacionActivity) getActivity();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final List<Subtarea> subtareaList = (ArrayList<Subtarea>) activity.getDatabase().subtareaDao().findAllByInstalacion(instalacion.getId());
                    subtareas.addAll(subtareaList);

                    if (!(instalacion.getSubtareas() == null)) {
                        subtareas.addAll(instalacion.getSubtareas());
                    }


                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            if (subtareas.size() < 1) {
                                recyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            } else {
                                recyclerView.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                            }
                        }
                    });

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.setListadesubtareas(subtareas);
                            activity.setSubtareasAdapter(new SubtareasRecyclerViewAdapter((ArrayList<Subtarea>) activity.getListadesubtareas(), mListener));
                            recyclerView.setAdapter(activity.getSubtareasAdapter());
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();


        ((InstalacionActivity) context).hiderogress();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SubtareasCallbackListener) {
            mListener = (SubtareasCallbackListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ComentariosCallbackListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
