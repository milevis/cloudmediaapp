package geolocaliza.example.com.geolocaliza;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* setContentView(R.layout.activity_web_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/



        try {

            setContentView(R.layout.activity_web_view);

            WebView webview = (WebView) findViewById(R.id.webview);
            // getWindow().requestFeature(Window.FEATURE_PROGRESS);

             webview.getSettings().setJavaScriptEnabled(true);


       /* final Activity activity = this;
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);
            }
        });
        webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });*/

            webview.loadUrl("http://201.245.200.58:2451/login.html");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                webview.evaluateJavascript("" +
                        "window.onload =  function() {" +
                        " document.getElementsById('login-password').value='admin'; }; " +
                        "var FunctionOne = function () {"
                        + "  try{document.getElementsById('login-password').value='admin';}catch(e){}"
                        + "};", null);
            }else{
                webview.loadUrl("javascript:"+
                        "window.onload =  function() {" +
                                " document.getElementsById('login-password').value='admin'; };"
                        + "var FunctionOne = function () {"
                        + "  try{document.getElementsById('login-password')[.value='admin';}catch(e){}"
                        + "};");
            }

        }
        catch (Exception e){
            Log.e("main activity", e.getMessage());
        }
    }



}
