package geolocaliza.example.com.geolocaliza.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.text.ParseException;

import geolocaliza.example.com.geolocaliza.InstalacionActivity;
import geolocaliza.example.com.geolocaliza.R;
import geolocaliza.example.com.geolocaliza.listeners.ComentariosCallbackListener;

/**
 * Created by Jhainey on 30/05/2018.
 */

public class AddCommentDialogFragment extends DialogFragment {
    private ComentariosCallbackListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View inflator = inflater.inflate(R.layout.add_comment, null);
        builder.setView(inflator);


        builder.setMessage(R.string.add_comment)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((InstalacionActivity)getActivity()).showProgress(getContext().getString(R.string.sending_comment));
                        EditText edit=(EditText)inflator.findViewById(R.id.comentario);
                        String text=edit.getText().toString();
                        try {
                            mListener.onListAddComment(text);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ComentariosCallbackListener) {
            mListener = (ComentariosCallbackListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ComentariosCallbackListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}