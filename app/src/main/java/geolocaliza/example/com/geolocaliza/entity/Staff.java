package geolocaliza.example.com.geolocaliza.entity;

/**
 * Created by Jhainey on 02/06/2018.
 */

public class Staff {
    private  int id;
    private String email;
    private int master_client;
    private String staffname;


    public Staff() {
    }

    public Staff(int id, String email, int master_client, String staffname) {
        this.id = id;
        this.email = email;
        this.master_client = master_client;
        this.staffname = staffname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getMaster_client() {
        return master_client;
    }

    public void setMaster_client(int master_client) {
        this.master_client = master_client;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }
}
