package geolocaliza.example.com.geolocaliza.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by Jhainey on 30/05/2018.
 */
@Entity( indices = {@Index("instalacion_id")}, foreignKeys = @ForeignKey(entity = Instalacion.class,
        parentColumns = "id",
        childColumns = "instalacion_id",
        onDelete = CASCADE))
public class Subtarea {
    @PrimaryKey
    private int id;
    private String description;
    private boolean finished;
    private boolean complete;
    private int instalacion_id;

    @Ignore
    public Subtarea(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public Subtarea(int id, String description, boolean finished, boolean complete, int instalacion_id) {
        this.id = id;
        this.description = description;
        this.finished = finished;
        this.complete = complete;
        this.instalacion_id = instalacion_id;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getInstalacion_id() {
        return instalacion_id;
    }

    public void setInstalacion_id(int instalacion_id) {
        this.instalacion_id = instalacion_id;
    }
}
