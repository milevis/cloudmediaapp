package geolocaliza.example.com.geolocaliza.services;

/**
 * Created by Jhainey on 18/05/2018.
 */

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import geolocaliza.example.com.geolocaliza.InstalacionActivity;
import geolocaliza.example.com.geolocaliza.LoginActivity;
import geolocaliza.example.com.geolocaliza.MainActivity;
import geolocaliza.example.com.geolocaliza.listeners.InternetReceiverListener;

public class InternetConnectionReceiver extends BroadcastReceiver {

    private static final String TAG = "NetworkStateReceiver";
    InternetReceiverListener mListener;
    String act;
    Context context;
    private boolean firstConection = false;

    public InternetConnectionReceiver(InternetReceiverListener list) {
        mListener = list;

    }


    public InternetConnectionReceiver() {
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        this.context =context;
        Log.d(TAG, "Network connectivity change");

        if(context instanceof LoginActivity){
            firstConection = ((LoginActivity)context).isFirstInternetCheck();
        }
        if(context instanceof MainActivity){
            firstConection = ((MainActivity)context).isFirstInternetCheck();
        }

        if(context instanceof InstalacionActivity){
            firstConection = ((InstalacionActivity)context).isFirstInternetCheck();
        }

        try {


            if (intent.getExtras() != null) {
                final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

                if (ni != null && ni.isConnected()) {
                    if (!firstConection) {
                        Log.i("Network", "Network connected");
                        updateFirstConn();
                        if (mListener != null) {
                            mListener.haveInternetListener();
                        }
                    }
                } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                    if (!firstConection) {
                        Log.d(TAG, "There's no network connectivity");
                        //updateFirstConn();
                        if (mListener != null) {
                            mListener.dontHaveInternetListener();
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }


    }

    private void updateFirstConn(){
        if(context instanceof LoginActivity){
            ((LoginActivity)context).setFirstInternetCheck(true);
        }
        if(context instanceof MainActivity){
            ((MainActivity)context).setFirstInternetCheck(true);
        }

        if(context instanceof InstalacionActivity){
           ((InstalacionActivity)context).setFirstInternetCheck(true);
        }

    }

}