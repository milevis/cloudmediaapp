package geolocaliza.example.com.geolocaliza.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import geolocaliza.example.com.geolocaliza.InstalacionActivity;
import geolocaliza.example.com.geolocaliza.R;
import geolocaliza.example.com.geolocaliza.adapters.FotosAdapter;
import geolocaliza.example.com.geolocaliza.entity.Foto;
import geolocaliza.example.com.geolocaliza.listeners.FotosLCallbackLIstener;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.entity.Staff;
import geolocaliza.example.com.geolocaliza.tasks.SendFotoTask;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p>
 * to handle interaction events.
 * Use the {@link FotosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FotosFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Instalacion instalacion;
    private Staff staff;
    private GridView imageGrid;


    private FotosLCallbackLIstener mListener;

    private String mCurrentPhotoPath;
    private TextView emptyView;
    private String photoName;
    private InstalacionActivity activity;

    public FotosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment FotosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FotosFragment newInstance(int columnCount) {
        FotosFragment fragment = new FotosFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        // new GetFotosDummyTask((FotosLCallbackLIstener) getActivity()).execute();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fotos_fragment, container, false);
        Context context = view.getContext();
        activity = ((InstalacionActivity) context);

        this.imageGrid = (GridView) view.findViewById(R.id.gridview);
        this.emptyView = (TextView) view.findViewById(R.id.empty_view);

        instalacion = ((InstalacionActivity) context).getInstalacion();
        staff = ((InstalacionActivity) context).getStaff();



        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.floating_add_foto);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final List<Foto> fotoList = (ArrayList<Foto>) activity.getDatabase().fotoDao().findAllByInstalacion(instalacion.getId());

                    if(!(instalacion.getImagenes() ==null)) {
                        activity.setBitmapList(instalacion.getImagenes());
                    }

                    for (Foto foto:
                         fotoList) {

                        Uri uri = activity.stringToUri("file://"+foto.getUrl());
                        Bitmap bitmap = activity.uriToBItmap(uri);
                        Bitmap scaled = activity.scaeImage(bitmap);
                        activity.getBitmapList().add(scaled);
                    }


                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            //INICIAR LA TAREA
                            activity.setFotosAdapter(new FotosAdapter(activity.getApplicationContext(), activity.getBitmapList()));
                            imageGrid.setAdapter(activity.getFotosAdapter());
                            if (activity.getBitmapList().isEmpty()) {
                                imageGrid.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            } else {

                                imageGrid.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
        activity.hiderogress();
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    private void openCamera() {
        // custom dialog

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "cloudapp");

        boolean can_read = true;
        if (!imagesFolder.exists()) {
            if (!imagesFolder.mkdirs()) {
                Log.d("cloudapp", "failed to create directory");
                can_read = false;
            }
        }


        if (can_read) {

            File image = new File(imagesFolder, "FOTO_" + timeStamp + ".jpg");
            Uri uriSavedImage = Uri.fromFile(image);

            // Uri uri = data.getData();

            mCurrentPhotoPath = "file:" + image.getAbsolutePath();

            Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
            photoName = image.getName();
            imageIntent.putExtra("return-data", true);
            startActivityForResult(imageIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        } else {
            ((InstalacionActivity) getActivity()).makeToast(String.valueOf(R.string.error_eternal_storage));
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 100:

                if (resultCode == Activity.RESULT_OK) {

                    try {

                        ((InstalacionActivity) getActivity()).showProgress(getContext().getString(R.string.sending_comment));
                        Bitmap bmp = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),
                                Uri.parse(mCurrentPhotoPath));


                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bmp.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                        //Convert the image to a string
                        byte[] byte_arr = stream.toByteArray();
                        String image_str = Base64.encodeToString(byte_arr, Base64.DEFAULT);


                        //escala la imagen

                        bmp = activity.scaeImage(bmp);


                        if (activity.getBitmapList().size() <= 0) {
                            imageGrid.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);

                        }


                        if (isNetworkAvailable()) {
                            final String  date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US).format(new Date());
                            new SendFotoTask((FotosLCallbackLIstener) this.getActivity(), instalacion.getId(), staff.getMaster_client(),
                                    image_str, bmp, photoName, date).execute();
                        } else {
                            Uri uri =  Uri.parse(mCurrentPhotoPath);
                            mListener.saveFotoOnDatabase(uri, photoName, bmp, image_str);
                        }

                    } catch (Exception e) {
                        Toast.makeText(this.getContext(),  R.string.error_take_picture, Toast.LENGTH_SHORT)
                                .show();
                        activity.hiderogress();
                        Log.e("Camera", e.toString());
                    }


                } else {
                    Toast.makeText(this.getContext(), R.string.error_take_picture, Toast.LENGTH_SHORT)
                            .show();
                }
        }
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSelectFoto(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FotosLCallbackLIstener) {
            mListener = (FotosLCallbackLIstener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
