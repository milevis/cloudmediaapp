package geolocaliza.example.com.geolocaliza.utils;

import android.arch.persistence.room.*;

import geolocaliza.example.com.geolocaliza.dao.ComentarioDao;
import geolocaliza.example.com.geolocaliza.dao.FotoDao;
import geolocaliza.example.com.geolocaliza.dao.InstalacionDao;
import geolocaliza.example.com.geolocaliza.dao.SubtareaDao;
import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Foto;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;


@Database(entities = {Instalacion.class, Subtarea.class, Comentario.class, Foto.class}, version = 11, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    public abstract InstalacionDao instalacionDao();
    public abstract SubtareaDao subtareaDao();
    public abstract ComentarioDao comentarioDao();
    public abstract FotoDao fotoDao();
}

