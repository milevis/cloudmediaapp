package geolocaliza.example.com.geolocaliza.tasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.listeners.InstalationCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.utils.Constants;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class GetInstalacionesTask extends AsyncTask<Double, Double,  ArrayList<Instalacion> > {

    private int user_id;
    private int master_client;
    private Exception exception;

    InstalationCallbackListener callbackInterface;

    public GetInstalacionesTask(InstalationCallbackListener callbackInterface, int user_id, int master_client) {

        super();
        this.callbackInterface = callbackInterface;
        this.user_id =user_id;
        this.master_client =master_client;
        // do stuff
    }

    @Override
    protected void onPostExecute( ArrayList<Instalacion>  result) {
        super.onPostExecute(result);
        this.callbackInterface.callbackGetInstalaciones(result);
    }

    private String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    @Override
    protected ArrayList<Instalacion>  doInBackground(Double... params) {

           StringBuffer response = new StringBuffer();



        try {
            String uristring = Constants.API_URL+"tasksInstalls/"+ user_id +"/"+master_client+"";
            URL url = new URL(uristring);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod("GET");

            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.flush();
            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'get' request to URL : " + url);

            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;


            int i = 0;
            while ((inputLine = in.readLine()) != null) {
                   response.append(inputLine);

                i++;
            }
            in.close();

            System.out.println(response.toString());


            writer.close();
            os.close();

            conn.connect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            return wrapData(response);
        }


    }

    private  ArrayList<Instalacion> wrapData(StringBuffer response){



        ArrayList<Instalacion> instalacions=new ArrayList<>();

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject objectFromString = jsonParser.parse(response.toString()).getAsJsonObject();


            for (JsonElement jsonElement :
                    objectFromString.get("instalaciones").getAsJsonArray()) {

              //  JsonElement datosinstalacion  =  jsonElement.getAsJsonObject().get("datosinstalacion");

                Instalacion instalacion = new Instalacion();
                int instalacion_id =Integer.parseInt(jsonElement.getAsJsonObject().get("id").toString().replace("\"", ""));
                instalacion.setId(instalacion_id);
                instalacion.setFecha_instalacion(jsonElement.getAsJsonObject().get("fecha_instalacion").toString().replace("\"", ""));

                instalacion.setNombre_servicio(jsonElement.getAsJsonObject().get("nombre_servicio").toString().replace("\"", ""));

                instalacion.setFechaCreacion(jsonElement.getAsJsonObject().get("creado").toString().replace("\"", ""));
                instalacion.setEstatus_plan(jsonElement.getAsJsonObject().get("estatus_plan").toString().replace("\"", ""));
                instalacion.setDireccion(jsonElement.getAsJsonObject().get("direccion").toString().replace("\"", ""));
                instalacion.setNameSurname(jsonElement.getAsJsonObject().get("name").toString().replace("\"", ""));
              //  instalacion.setApellidoResidencial(jsonElement.getAsJsonObject().get("apellido_residencial").toString().replace("\"", ""));
              //  instalacion.setCompany(jsonElement.getAsJsonObject().get("company").toString().replace("\"", ""));

                JsonObject datosinstalacion = (JsonObject) jsonElement.getAsJsonObject().get("datosinstalacion");
                JsonObject task = (JsonObject) datosinstalacion.getAsJsonObject().get("task");

                instalacion.setFechaInstalado(task.getAsJsonObject().get("fecha_instalado").toString().replace("\"", ""));
                instalacion.setFecha_instalacion(task.getAsJsonObject().get("fecha_instalacion").toString().replace("\"", ""));
                //instalacion.setType(Integer.parseInt(jsonElement.getAsJsonObject().get("type").toString().replace("\"", "")));

               // instalacion.setLatitud(Double.parseDouble(jsonElement.getAsJsonObject().get("latitud_referencia").toString().replace("\"", "")));
                //instalacion.setLogitud(Double.parseDouble(jsonElement.getAsJsonObject().get("longitud_referencia").toString().replace("\"", "")));



                //Wrapeo los comentarios
                ArrayList<Comentario> comentarios = new ArrayList<>();
                for (JsonElement comentarioJson :
                        datosinstalacion.get("comentarios").getAsJsonArray()) {
                    String comentarioDescription = comentarioJson.getAsJsonObject().get("comentario").toString().replace("\"", "");
                    String staffname = comentarioJson.getAsJsonObject().get("staffname").toString().replace("\"", "");
                    int id = Integer.parseInt(comentarioJson.getAsJsonObject().get("id").toString().replace("\"", ""));
                    String fecha = comentarioJson.getAsJsonObject().get("fecha_comentario").toString().replace("\"", "");
                    Comentario comentario = new Comentario(id
                            , comentarioDescription, fecha, staffname, instalacion_id);

                    comentarios.add(comentario);
                }
                instalacion.setComentarios(comentarios);

                //Wrapeo los comentarios
                ArrayList<Subtarea> subtareas = new ArrayList<>();
                for (JsonElement subtareaJson :
                        datosinstalacion.get("subtareas").getAsJsonArray()) {
                    String description = subtareaJson.getAsJsonObject().get("description").toString().replace("\"", "");
                    int id = Integer.parseInt(subtareaJson.getAsJsonObject().get("id").toString().replace("\"", ""));
                    int completeInt = Integer.valueOf(subtareaJson.getAsJsonObject().get("complete").toString().replace("\"", ""));
                    int finishedInt = Integer.valueOf(subtareaJson.getAsJsonObject().get("finished").toString().replace("\"", ""));
                    boolean finished = finishedInt==1?true:false;
                    boolean complete = completeInt==1?true:false;
                    Subtarea subtarea = new Subtarea(id
                            ,description, finished, complete, instalacion_id);

                    subtareas.add(subtarea);
                }
                instalacion.setSubtareas(subtareas);


               /* for (JsonElement fotoJson :
                        datosinstalacion.get("files").getAsJsonArray()) {
                    try {
                        String name = fotoJson.getAsJsonObject().get("name").toString().replace("\"", "");
                        Bitmap bitmap = urlImageToBitmap("https://cloudmedia.co/crmdesarrollo/uploads/files/"+name);

                        int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
                        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                        instalacion.getImagenes().add(scaled);

                    }catch (Exception e){
                        Log.e("error leyendo la imagen", e.getMessage());
                    }

                }*/


                instalacions.add(instalacion);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            return  instalacions;
        }



    }
}