package geolocaliza.example.com.geolocaliza;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import geolocaliza.example.com.geolocaliza.adapters.InstalacionesRecyclerViewAdapter;
import geolocaliza.example.com.geolocaliza.entity.Comentario;
import geolocaliza.example.com.geolocaliza.entity.Foto;
import geolocaliza.example.com.geolocaliza.entity.Subtarea;
import geolocaliza.example.com.geolocaliza.fragments.InternetDialogFragment;
import geolocaliza.example.com.geolocaliza.listeners.ComentariosCallbackListener;
import geolocaliza.example.com.geolocaliza.listeners.FotosLCallbackLIstener;
import geolocaliza.example.com.geolocaliza.listeners.InstalationCallbackListener;
import geolocaliza.example.com.geolocaliza.entity.Instalacion;
import geolocaliza.example.com.geolocaliza.entity.Staff;
import geolocaliza.example.com.geolocaliza.listeners.InternetReceiverListener;
import geolocaliza.example.com.geolocaliza.listeners.SubtareasCallbackListener;
import geolocaliza.example.com.geolocaliza.services.InternetConnectionReceiver;
import geolocaliza.example.com.geolocaliza.services.SendDataService;
import geolocaliza.example.com.geolocaliza.services.BootReceiverService;
import geolocaliza.example.com.geolocaliza.tasks.GetInstalacionesTask;
import geolocaliza.example.com.geolocaliza.tasks.SendLIstUpdateSubtareaTask;
import geolocaliza.example.com.geolocaliza.tasks.SendListCommentTask;
import geolocaliza.example.com.geolocaliza.tasks.SendListFotoTask;
import geolocaliza.example.com.geolocaliza.tasks.SendListUpdateInstalacionTask;
import geolocaliza.example.com.geolocaliza.utils.AppDatabase;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity implements InstalationCallbackListener,
        NavigationView.OnNavigationItemSelectedListener, ComentariosCallbackListener, FotosLCallbackLIstener, SubtareasCallbackListener,
        InternetReceiverListener , LocationListener {
    private Intent intentService;

    private ArrayList<Instalacion> instalaciones;
    private Staff staff = new Staff();

    AppDatabase database;
    SharedPreferences sharedPref;
    private static final String DATABASE_NAME = "cloud_app";
    private ProgressDialog nDialog;
    List<Comentario> comentariostoSync;
    List<Foto> fotostoSync;
    List<Subtarea> subtareasToSync;

    private BootReceiverService serviceBootReceiver;
    private InternetConnectionReceiver internetConnectionReceiver;
    private boolean firstInternetCheck = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nDialog = new ProgressDialog(MainActivity.this);

        database = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).build();

        setContentView(R.layout.activity_main);
        setSharedPreferences();

        TextView_Lato textoBienvenido  = (TextView_Lato) findViewById(R.id.textoBienvenido);
        Textview_lato_thin emailStaff  = (Textview_lato_thin) findViewById(R.id.emailStaff);


        textoBienvenido.setText(staff.getStaffname());
        emailStaff.setText(staff.getEmail());

       // setNavigation();
        mostrarInstalaciones();
        setReceivers();

        //startServiceCron();
    }

    private void setReceivers() {

        setFirstInternetCheck(true);
        serviceBootReceiver = new BootReceiverService();
        internetConnectionReceiver = new InternetConnectionReceiver(this);
        //en vesiones nuevas no hace falta , asta cn el manifiest
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(internetConnectionReceiver, filter);


        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction("ac.in.ActivityRecognition.RestartSensor");
        //     screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        //   screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(serviceBootReceiver, screenStateFilter);

    }

    public void showProgress(String text) {
        nDialog.setMessage("Por favor espere..");
        nDialog.setTitle(text);
        nDialog.setIndeterminate(false);
        nDialog.setCancelable(true);
        nDialog.show();
    }

    public void hiderogress() {
        nDialog.dismiss();
    }


    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(serviceBootReceiver);
            unregisterReceiver(internetConnectionReceiver);
        } catch (Exception e) {
            Log.e("main activity", e.getMessage());
        }
    }

    private void mostrarInstalaciones() {
        showProgress("Buscando instalaciones");
        if (isNetworkAvailable()) {
            new GetInstalacionesTask(this, staff.getId(), staff.getMaster_client()).execute();
        } else {


            new Thread(new Runnable() {
                @Override
                public void run() {
                    instalaciones = (ArrayList<Instalacion>) database.instalacionDao().getAll();
                    renderREcyclerViewInstalaciones();
                }
            }).start();
            hiderogress();
        }

    }

    public void makeToast(String textto) {
        Context context = getApplicationContext();
        CharSequence text = textto;
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

    }

    public boolean isFirstInternetCheck() {
        return firstInternetCheck;
    }

    public void setFirstInternetCheck(boolean firstInternetCheck) {
        this.firstInternetCheck = firstInternetCheck;
    }

    private void setActionBar() {
        // getSupportActionBar().setTitle("Instalaciones");
    }

    private void setNavigation() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle("");

       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

      //  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
       // navigationView.setNavigationItemSelectedListener(this);

    }

    public void showInternetDiaglog() {
        DialogFragment newFragment = new InternetDialogFragment();
        newFragment.show(this.getSupportFragmentManager(), "internet_connection");
    }

    public void startServiceCron() {


        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION},
                    1);


            return;
        } else {

            double latitude = 0;
            double longitude = 0;


            LocationManager locationManager = (LocationManager) getApplicationContext()
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            Boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            Boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
                makeToast("GPS o Internet no disponible");
            } else {

                Location location = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            10,
                            10, this);
                    Log.d("Network", "Network Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                2,
                                2, this);
                        Log.d("GPS", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

            // Add a marker in Sydney and move the camera

            intentService = new Intent(this, SendDataService.class);
            intentService.putExtra("latitude", latitude);
            intentService.putExtra("longitude", longitude);
            intentService.putExtra("email", staff.getEmail());
            intentService.putExtra("name", staff.getStaffname());
            intentService.putExtra("stafId", staff.getId());
            intentService.putExtra("master_client", staff.getMaster_client());
            intentService.setAction("SENDDATA");
            startService(intentService);

        }


    }

    public Intent getIntentService() {
        return intentService;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    startServiceCron();


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    makeToast("No tiene Gps Habilitado, imposible compartir ubicación!");


                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                //  if(isTaskRoot()) {
                   /* Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                    homeIntent.addCategory(Intent.CATEGORY_HOME);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeIntent);*/
                moveTaskToBack(true);
                return false;
               /* }
                else {
                    //super.keyDown(keyCode,event);
                    return false;
                }*/

            default:
                //  super.keyDown(keyCode,event);
                return false;
        }

    }

    private void renderREcyclerViewInstalaciones() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.instalaciones_list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        recyclerView.setAdapter(new InstalacionesRecyclerViewAdapter(instalaciones, this));

        hiderogress();
    }

    @Override
    public void callbackGetInstalacion(Instalacion result) {

    }

    @Override
    public void callbackGetInstalaciones(ArrayList<Instalacion> result) {

        instalaciones = result;
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Subtarea> subtareas = new ArrayList<>();
                List<Comentario> comentarios = new ArrayList<>();
                database.instalacionDao().insertAll(instalaciones);
                for (Instalacion instalacion :
                        instalaciones) {

                    subtareas.addAll(instalacion.getSubtareas());
                    comentarios.addAll(instalacion.getComentarios());

                }

                database.subtareaDao().insertAll(subtareas);
                database.comentarioDao().insertAll(comentarios);
            }
        }).start();
        renderREcyclerViewInstalaciones();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void callbackClickInstalacion(Instalacion result, int position) {

        Intent mainIntent = new Intent().setClass(MainActivity.this, InstalacionActivity.class);
        mainIntent.putExtra("instalacion_id", result.getId());
        // mainIntent.putExtra("instalacion", result);

        startActivity(mainIntent);
    }

    @Override
    public void callbackClickUpdateInstalacionStatus(Boolean result, String estatus) {

    }

    @Override
    public void callbackClickUpdateAddressClient(Boolean result) {

    }

    @Override
    public void callbackInternetConected(Boolean result) {
        sincronizarComentarios();
    }

    @Override
    public void callbackSIncronizeInstalaciones(Boolean subtarea, ArrayList<Instalacion> instalacions) {

        makeToast("Datos sincronizados");

        mostrarInstalaciones();
    }

    private void setSharedPreferences() {

        sharedPref = getSharedPreferences("STAFF", 0);
        staff.setEmail(sharedPref.getString("EMAIL", ""));
        staff.setStaffname(sharedPref.getString("NAME", ""));
        staff.setId(sharedPref.getInt("ID", 0));
        staff.setMaster_client(sharedPref.getInt("MASTER_CLIENT", 0));


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sync) {
            sincronizarComentarios();


        }



        return super.onOptionsItemSelected(item);
    }



    public void sincronizarComentarios() {
        if (isNetworkAvailable()) {

            final ComentariosCallbackListener comentariosCallbackListener = this;

            showProgress("Enviando data al servidor ");
            new Thread(new Runnable() {
                @Override
                public void run() {

                    comentariostoSync = (ArrayList<Comentario>) database.comentarioDao().getAll();
                    new SendListCommentTask(comentariosCallbackListener, (ArrayList<Comentario>) comentariostoSync,
                            staff.getId(), staff.getMaster_client()).execute();


                }
            }).start();


        } else {
            makeToast("No hay conexión a internet");
        }
    }


    public void sincronizarFotos() {
        if (isNetworkAvailable()) {

            final FotosLCallbackLIstener fotosLCallbackLIstener = this;

            showProgress("Enviando data al servidor ");
            new Thread(new Runnable() {
                @Override
                public void run() {


                    fotostoSync = (ArrayList<Foto>) database.fotoDao().getAll();

                    new SendListFotoTask(fotosLCallbackLIstener, (ArrayList<Foto>) fotostoSync,
                            staff.getId(), staff.getMaster_client()).execute();


                }
            }).start();


        } else {
            makeToast("No hay conexión a internet");
        }
    }


    public void sincronizarSubtareas() {
        if (isNetworkAvailable()) {

            final SubtareasCallbackListener subtareasCallbackListener = (SubtareasCallbackListener) this;

            showProgress("Enviando data al servidor ");
            new Thread(new Runnable() {
                @Override
                public void run() {


                    subtareasToSync = (ArrayList<Subtarea>) database.subtareaDao().getAll();
                    new SendLIstUpdateSubtareaTask(subtareasCallbackListener, (ArrayList<Subtarea>) subtareasToSync,
                            staff.getId(), staff.getMaster_client()).execute();


                }
            }).start();


        } else {
            makeToast("No hay conexión a internet");
        }
    }


    public void sincronizarInstalaciones() {
        if (isNetworkAvailable()) {

            final InstalationCallbackListener callbackListener = (InstalationCallbackListener) this;

            showProgress("Enviando data al servidor ");
            new Thread(new Runnable() {
                @Override
                public void run() {


                    List<Instalacion> instalacionestoSYnc = (ArrayList<Instalacion>) database.instalacionDao().getAll();
                    new SendListUpdateInstalacionTask(callbackListener, (ArrayList<Instalacion>) instalacionestoSYnc,
                            staff.getId(), staff.getMaster_client()).execute();


                }
            }).start();


        } else {
            makeToast("No hay conexión a internet");
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {


        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClickComentarioItem(Comentario item) {

    }

    @Override
    public void onListAddComment(String comment) {

    }

    @Override
    public void callbackSendPostComment(Boolean result, ArrayList<Comentario> comentarios) {


    }

    @Override
    public void callbackSyncComents(Boolean result, final ArrayList<Comentario> comentarios) {


        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Comentario comentario : comentarios) {
                    database.comentarioDao().delete(comentario);
                }

            }
        }).start();

        sincronizarFotos();


    }


    @Override
    public void onSelectFoto(Uri uri) {

    }

    @Override
    public void ongetFotos(ArrayList<Bitmap> fotos) {

    }

    @Override
    public void onUploadFoto(Boolean result, Bitmap bmp) {

    }

    @Override
    public void saveFotoOnDatabase(Uri uri, String photoName, Bitmap bmp, String image_str) {

    }

    @Override
    public void callbackSyncFotos(Boolean result, final ArrayList<Foto> fotos) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Foto foto : fotos) {
                    database.fotoDao().delete(foto);
                }

            }
        }).start();
        sincronizarSubtareas();

    }

    @Override
    public void onClickSubTareaItem(Subtarea item) {

    }

    @Override
    public void callbackUpdateSubtask(Boolean result, ArrayList<Subtarea> subtareas) {

    }

    @Override
    public void callbackClickComplete(Subtarea subtarea, int position) {

    }

    @Override
    public void callbackClickUnComplete(Subtarea subtarea, int position) {

    }

    @Override
    public void callbackSIncronizeSubtasks(Boolean subtarea, ArrayList<Subtarea> subtareasudates) {
        sincronizarInstalaciones();

    }

    @Override
    public void haveInternetListener() {
        makeToast(getString(R.string.internet));
        // ((MainActivity) context).showInternetDiaglog();
        sincronizarComentarios();

        setFirstInternetCheck(true);
    }

    @Override
    public void dontHaveInternetListener() {
    makeToast(getString(R.string.no_internet));
    }

    @Override
    public void onLocationChanged(Location location) {
        
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}

