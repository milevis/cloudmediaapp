package geolocaliza.example.com.geolocaliza.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by Jhainey on 30/05/2018.
 */

@Entity(indices = {@Index("instalacion_id")}, foreignKeys = @ForeignKey(entity = Instalacion.class,
        parentColumns = "id",
        childColumns = "instalacion_id",
        onDelete = CASCADE))
public class Comentario {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String comentario;
    private String fecha_comentario;
    private String staffname;
    private int instalacion_id;
    private int staff_id;

    public Comentario(int id, String comentario, String fecha_comentario, String staffname, int instalacion_id) {
        this.id = id;
        this.comentario = comentario;
        this.fecha_comentario = fecha_comentario;
        this.staffname = staffname;
        this.instalacion_id = instalacion_id;
    }

    @Ignore
    public Comentario(String comentario, String fecha_comentario, String staffname, int instalacion_id, int staff_id) {
        this.comentario = comentario;
        this.fecha_comentario = fecha_comentario;
        this.staffname = staffname;
        this.instalacion_id = instalacion_id;
        this.staff_id = staff_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getFecha_comentario() {
        return fecha_comentario;
    }

    public void setFecha_comentario(String fecha_comentario) {
        this.fecha_comentario = fecha_comentario;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public String getStaffname() {
        return staffname;
    }

    public void setStaffname(String staffname) {
        this.staffname = staffname;
    }

    public int getInstalacion_id() {
        return instalacion_id;
    }

    public void setInstalacion_id(int instalacion_id) {
        this.instalacion_id = instalacion_id;
    }
}
