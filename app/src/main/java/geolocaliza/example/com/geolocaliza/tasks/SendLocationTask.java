package geolocaliza.example.com.geolocaliza.tasks;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import geolocaliza.example.com.geolocaliza.utils.Constants;

/**
 * Created by Jhainey on 18/05/2018.
 */

public class SendLocationTask extends AsyncTask<Double, Double, Boolean> {

    private String email;
    private int id;
    private int master_client;

    private Exception exception;

    public SendLocationTask(String email, int id, int master_client) {

        super();
        this.email =email;
        this.id =id;
        this.master_client =master_client;

        // do stuff
    }

    private String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while (itr.hasNext()) {

            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    protected void onPostExecute(Boolean result) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }


    @Override
    protected Boolean doInBackground(Double... params) {

        Double latitude = params[0];
        Double longitude = params[1];

        try {
            URL url = new URL(Constants.API_URL+"updateposition/");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);

            conn.setRequestMethod("POST");

            conn.setDoInput(true);
            conn.setDoOutput(true);
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("lat", latitude);
            postDataParams.put("lng", longitude);
            postDataParams.put("email", email);
            postDataParams.put("id", id);
            postDataParams.put("master_client", master_client);



            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();


            int responseCode = conn.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + getPostDataString(postDataParams));
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println(response.toString());


            writer.close();
            os.close();

            conn.connect();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            return  true;
        }

    }
}